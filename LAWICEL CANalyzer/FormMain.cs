﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace LAWICEL_CANalyzer
{
    public partial class FormMain : Form
    {
        private ToolTip tooltip = new System.Windows.Forms.ToolTip();
        private CANusb_Connector connector;
        private List<List<CANCommand>> cmdList;
        private List<List<CANCommand>> fixedCmdList;
        private List<ThreadTestVita> threadsTestVita;
        private List<Range<int>> listIdRangePass;
        private Range<int> idRangePass;
        private List<Range<int>> listIdRangeStop;
        private Range<int> idRangeStop;
        private TimeSpan timeSpanVita;
        private DateTime timeStartTest;

        delegate void CANCommandArgReturningVoidDelegate(DataGridView dgv, CANCommand msg);
        public FormMain()
        {
            InitializeComponent();
            setActiveDongles();
            setToolTips();
            listIdRangePass = new List<Range<int>>();
            listIdRangeStop = new List<Range<int>>();
            idRangePass = new Range<int>();
            idRangePass.Minimum = 0x000;
            idRangePass.Maximum = 0xFFF;
            listIdRangePass.Add(idRangePass);
            initializeComponentsForm();
        }

        private void setActiveDongles()
        {
            try
            {
                List<String> dongleList = new List<string>();
                dongleList = Utility.getCanUSBList();
                cmb_InterfaceList.Items.Clear();
                if (dongleList != null && !dongleList.Count.Equals(0))
                {
                    foreach (string d in dongleList)
                        cmb_InterfaceList.Items.Add(d);
                    cmb_InterfaceList.BackColor = Color.White;
                    cmb_InterfaceList.SelectedIndex = 0;
                }
                else
                {
                    cmb_InterfaceList.BackColor = Color.Salmon;
                    cmb_InterfaceList.SelectedIndex = -1;
                    cmb_InterfaceList.Text = "";
                    cmb_InterfaceList.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void initializeComponentsForm()
        {
            try
            {
                tssl_Status.Text = "Ready";
                tssl_Time.Text = DateTime.Now.ToString("00:00:00:00");
                lbl_TxFrames.Text = "0";
                lbl_TxRemoteFrames.Text = "0";
                lbl_RxFrames.Text = "0";
                lbl_RxRemoteFrames.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            setActiveDongles();
        }

        private void setToolTips()
        {
            tooltip.SetToolTip(btn_Submit, "Submit Message");
        }

        private void btn_OnOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_OnOff.Tag.Equals("On"))
                {
                    if (cmb_InterfaceList.SelectedItem == null)
                        MessageBox.Show("Select a valid CANUSB Device", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else if (cmb_BaudRate.SelectedItem == null)
                        MessageBox.Show("Select a valid Baud Rate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        connector = new CANusb_Connector(cmb_InterfaceList.SelectedItem.ToString(), cmb_BaudRate.SelectedItem.ToString());
                        if (connector.IsConnected)
                        {
                            //Connessione OK
                            connector.Handler += ConnectorOnHandler();
                            btn_OnOff.Tag = "Off";
                            cmb_InterfaceList.Enabled = false;
                            cmb_BaudRate.Enabled = false;
                            btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.offIcon;
                            cmdList = new List<List<CANCommand>>();
                            fixedCmdList = new List<List<CANCommand>>();
                            grp_SendMessage.Enabled = true;
                            grp_Scripts.Enabled = true;
                            grp_controlPanel.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Connection to the device failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            setActiveDongles();
                        }
                    }
                }
                else if (btn_OnOff.Tag.Equals("Off"))
                {
                    connector.CANusb_disconnect();
                    if (!connector.IsConnected)
                    {
                        //Disconnessione OK
                        btn_OnOff.Tag = "On";
                        cmb_InterfaceList.Enabled = true;
                        cmb_BaudRate.Enabled = true;
                        btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.onIcon;
                        grp_SendMessage.Enabled = false;
                        grp_Scripts.Enabled = false;
                        grp_controlPanel.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("Disconnection from the device failed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        setActiveDongles();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Clean_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_CANTrace.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Filter_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Filter(listIdRangePass, listIdRangeStop))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_Highightning_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Feature Not Available!!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Action<CANCommand> ConnectorOnHandler()
        {
            return delegate (CANCommand value) { this.EmitMessageChange(value); };
        }

        private void EmitMessageChange(CANCommand message)
        {
            InsertIntoDataGridView(dgv_CANTrace, message);
        }

        private void InsertIntoDataGridView(DataGridView dgv, CANCommand msg)
        {
            try
            {
                // InvokeRequired required compares the thread ID of the  
                // calling thread to the thread ID of the creating thread.  
                // If these threads are different, it returns true.
                if (dgv.InvokeRequired)
                {
                    CANCommandArgReturningVoidDelegate d = new CANCommandArgReturningVoidDelegate(InsertIntoDataGridView);
                    this.Invoke(d, new object[] { dgv, msg });
                }
                else
                {
                    foreach (Range<int> r in listIdRangePass)
                        if(r.ContainsValue(Convert.ToInt32(msg.ID)))
                        {
                            dgv.Rows.Insert(0, msg.ActDateTime.ToString("HH:mm:ss:ffff"), msg.MessageType == 0 ? "Rx" : "Tx", msg.ID.ToString("X3"), msg.DLC, msg.Flags, msg.DataBytes.d0.ToString("X2"), msg.DataBytes.d1.ToString("X2"), msg.DataBytes.d2.ToString("X2"), msg.DataBytes.d3.ToString("X2"), msg.DataBytes.d4.ToString("X2"), msg.DataBytes.d5.ToString("X2"), msg.DataBytes.d6.ToString("X2"), msg.DataBytes.d7.ToString("X2"), msg.TimeStamp);
                            switch (msg.MessageType)
                            {
                                case 0://Rx
                                    switch (msg.Flags)
                                    {
                                        case 0:
                                            lbl_RxFrames.Text = (Convert.ToInt32(lbl_RxFrames.Text) + 1).ToString();
                                            break;
                                        default://Remote
                                            lbl_RxRemoteFrames.Text = (Convert.ToInt32(lbl_RxRemoteFrames.Text) + 1).ToString();
                                            break;
                                    }
                                    break;
                                case 1://Tx
                                    switch (msg.Flags)
                                    {
                                        case 0:
                                            lbl_TxFrames.Text = (Convert.ToInt32(lbl_TxFrames.Text) + 1).ToString();
                                            break;
                                        default://Remote
                                            lbl_TxRemoteFrames.Text = (Convert.ToInt32(lbl_TxRemoteFrames.Text) + 1).ToString();
                                            break;
                                    }
                                    break;
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Control Panel

        private void btn_createCmdList_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_FileAddEdit("Create Command List"))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_selectCmdList_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_FileSelect(@"\Commands"))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        fixedCmdList = Utility.loadCanCommandFile(form.files, @"\Commands");
                        initializeFixedMessagePanel();
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void initializeFixedMessagePanel()
        {
            try
            {
                fixedCmdList[0].Reverse();
                tb_msgSend1.Text = fixedCmdList[0][0].Description;
                tb_msgSend2.Text = fixedCmdList[0][1].Description;
                tb_msgSend3.Text = fixedCmdList[0][2].Description;
                tb_msgSend4.Text = fixedCmdList[0][3].Description;
                tb_msgSend5.Text = fixedCmdList[0][4].Description;
                tb_msgSend6.Text = fixedCmdList[0][5].Description;
                tb_msgSend7.Text = fixedCmdList[0][6].Description;
                tb_msgSend8.Text = fixedCmdList[0][7].Description;
                tb_msgSend9.Text = fixedCmdList[0][8].Description;
                tb_msgSend10.Text = fixedCmdList[0][9].Description;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_editCmdList_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    using (var form = new Form_FileAddEdit("Edit Command List"))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            form.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region comandiTest

        private void sendFixedCommand(int idx)
        {
            try
            {
                CANCommand msg = new CANCommand(fixedCmdList[0][idx].ID, fixedCmdList[0][idx].DLC, fixedCmdList[0][idx].Flags, fixedCmdList[0][idx].DataBytes.d0, fixedCmdList[0][idx].DataBytes.d1, fixedCmdList[0][idx].DataBytes.d2, fixedCmdList[0][idx].DataBytes.d3, fixedCmdList[0][idx].DataBytes.d4, fixedCmdList[0][idx].DataBytes.d5, fixedCmdList[0][idx].DataBytes.d6, fixedCmdList[0][idx].DataBytes.d7);
                connector.enqueueMessage(msg);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Btn_send1_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send2_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send3_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send4_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send5_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(4);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send6_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(5);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send7_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(6);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send8_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(7);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send9_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(8);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_send10_Click(object sender, EventArgs e)
        {
            try
            {
                sendFixedCommand(9);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region SendMessage

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            CANCommand msg = new CANCommand(Convert.ToUInt32(Utility.hexValidator(tb_ID.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_DLC.Text, 1), 16), cb_RTR.Checked ? (byte)0x01 : (byte)0x00, Convert.ToByte(Utility.hexValidator(tb_D0.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D1.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D2.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D3.Text, 3), 16), Convert.ToByte(Utility.hexValidator(tb_D4.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D5.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D6.Text, 2), 16), Convert.ToByte(Utility.hexValidator(tb_D7.Text, 2), 16));
            connector.enqueueMessage(msg);
        }

        #endregion

        #region Scripts

        private void btn_createScript_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_FileAddEdit("Create Script"))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_selectScripts_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_FileSelect(@"\Scripts"))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        cmdList = Utility.loadCanCommandFile(form.files, @"\Scripts");
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_editScript_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    using (var form = new Form_FileAddEdit("Edit Script"))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            form.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_startScripts_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmdList != null && !cmdList.Count.Equals(0))
                {
                    if (btn_startScripts.Tag.Equals("Start"))
                    {
                        btn_startScripts.Tag = "Stop";
                        btn_startScripts.Image = imageList.Images[3];
                        threadsTestVita = creaTestVita(cmdList);
                        foreach (ThreadTestVita item in threadsTestVita)
                        {
                            item.thread.Start();
                        }
                        tssl_Status.Text = "Script Started";
                        timeStartTest = DateTime.Now;
                        timeSpanVita = new TimeSpan(timeStartTest.Day, timeStartTest.Hour, timeStartTest.Minute, timeStartTest.Second);
                        tssl_Time.Text = DateTime.Now.ToString("00:00:00:00");
                        timerVita.Start();
                    }
                    else if (btn_startScripts.Tag.Equals("Stop"))
                    {
                        btn_startScripts.Tag = "Start";
                        btn_startScripts.Image = imageList.Images[2];
                        foreach (ThreadTestVita item in threadsTestVita)
                        {
                            item.stopThread();
                        }
                        timerVita.Stop();
                        tssl_Status.Text = "Ready";
                    }
                }
                else
                {
                    MessageBox.Show("Select a valid script file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<ThreadTestVita> creaTestVita(List<List<CANCommand>> cmdList)
        {
            ThreadTestVita threadTestVita;
            List<ThreadTestVita> lista = new List<ThreadTestVita>();

            int scriptNumber = 0;
            foreach (List<CANCommand> list in cmdList)
            {
                threadTestVita = new ThreadTestVita(connector, list, scriptNumber);
                threadTestVita.thread = new Thread(new ThreadStart(threadTestVita.startThread));
                threadTestVita.thread.IsBackground = true;
                lista.Add(threadTestVita);
                scriptNumber++;
            }
            return lista;
        }

        private void TimerVita_Tick(object sender, EventArgs e)
        {
            try
            {
                TimeSpan ts = new TimeSpan(DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                tssl_Time.Text = timeSpanVita.Subtract(ts).ToString("dd\\:hh\\:mm\\:ss");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region focusMaskedtextBox

        private void tb_ID_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_ID.SelectAll();
        }

        private void tb_DLC_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_DLC.SelectAll();
        }

        private void tb_D0_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D0.SelectAll();
        }

        private void tb_D1_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D1.SelectAll();
        }

        private void tb_D2_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D2.SelectAll();
        }

        private void tb_D3_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D3.SelectAll();
        }

        private void tb_D4_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D4.SelectAll();
        }

        private void tb_D5_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D5.SelectAll();
        }

        private void tb_D6_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D6.SelectAll();
        }

        private void tb_D7_Enter(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate { SetMaskedTextBoxSelectAll((MaskedTextBox)sender); }); tb_D7.SelectAll();
        }

        private void SetMaskedTextBoxSelectAll(MaskedTextBox txtbox)
        {
            txtbox.SelectAll();
        }

        #endregion

    }
}
