﻿namespace LAWICEL_CANalyzer
{
    partial class Form_FileAddEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_FileAddEdit));
            this.toolStripMainFunc = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tb_fileName = new System.Windows.Forms.ToolStripTextBox();
            this.btn_Open = new System.Windows.Forms.ToolStripButton();
            this.btn_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddMessages = new System.Windows.Forms.ToolStrip();
            this.btn_AddMessage = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteMessage = new System.Windows.Forms.ToolStripButton();
            this.dgv_CANMessages = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripMainFunc.SuspendLayout();
            this.toolStripAddMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANMessages)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripMainFunc
            // 
            this.toolStripMainFunc.AutoSize = false;
            this.toolStripMainFunc.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripMainFunc.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMainFunc.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStripMainFunc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tb_fileName,
            this.btn_Open,
            this.btn_Save});
            this.toolStripMainFunc.Location = new System.Drawing.Point(0, 0);
            this.toolStripMainFunc.Name = "toolStripMainFunc";
            this.toolStripMainFunc.Size = new System.Drawing.Size(712, 50);
            this.toolStripMainFunc.TabIndex = 10;
            this.toolStripMainFunc.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(60, 47);
            this.toolStripLabel1.Text = "File Name";
            // 
            // tb_fileName
            // 
            this.tb_fileName.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_fileName.Name = "tb_fileName";
            this.tb_fileName.Size = new System.Drawing.Size(200, 50);
            this.tb_fileName.Text = "File Name";
            this.tb_fileName.Leave += new System.EventHandler(this.tb_fileName_Leave);
            this.tb_fileName.Click += new System.EventHandler(this.tb_fileName_Click);
            // 
            // btn_Open
            // 
            this.btn_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Open.Image = ((System.Drawing.Image)(resources.GetObject("btn_Open.Image")));
            this.btn_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.Size = new System.Drawing.Size(51, 47);
            this.btn_Open.Text = "toolStripButton1";
            this.btn_Open.ToolTipText = "Open Script";
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Save.Image = global::LAWICEL_CANalyzer.Properties.Resources.save;
            this.btn_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(51, 47);
            this.btn_Save.Text = "Save Script";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // toolStripAddMessages
            // 
            this.toolStripAddMessages.AutoSize = false;
            this.toolStripAddMessages.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripAddMessages.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripAddMessages.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStripAddMessages.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_AddMessage,
            this.btn_DeleteMessage});
            this.toolStripAddMessages.Location = new System.Drawing.Point(0, 50);
            this.toolStripAddMessages.Name = "toolStripAddMessages";
            this.toolStripAddMessages.Size = new System.Drawing.Size(712, 50);
            this.toolStripAddMessages.TabIndex = 11;
            this.toolStripAddMessages.Text = "toolStrip2";
            // 
            // btn_AddMessage
            // 
            this.btn_AddMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddMessage.Image = global::LAWICEL_CANalyzer.Properties.Resources.add;
            this.btn_AddMessage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddMessage.Name = "btn_AddMessage";
            this.btn_AddMessage.Size = new System.Drawing.Size(51, 47);
            this.btn_AddMessage.Text = "Add Message";
            this.btn_AddMessage.Click += new System.EventHandler(this.btn_AddMessage_Click);
            // 
            // btn_DeleteMessage
            // 
            this.btn_DeleteMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteMessage.Image = global::LAWICEL_CANalyzer.Properties.Resources.delete;
            this.btn_DeleteMessage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteMessage.Name = "btn_DeleteMessage";
            this.btn_DeleteMessage.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteMessage.Text = "Remove Message";
            this.btn_DeleteMessage.Click += new System.EventHandler(this.btn_DeleteMessage_Click);
            // 
            // dgv_CANMessages
            // 
            this.dgv_CANMessages.AllowUserToAddRows = false;
            this.dgv_CANMessages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CANMessages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.DLC,
            this.RTR,
            this.D0,
            this.D1,
            this.D2,
            this.D3,
            this.D4,
            this.D5,
            this.D6,
            this.D7,
            this.Timestamp,
            this.description});
            this.dgv_CANMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_CANMessages.Location = new System.Drawing.Point(0, 100);
            this.dgv_CANMessages.Name = "dgv_CANMessages";
            this.dgv_CANMessages.Size = new System.Drawing.Size(712, 537);
            this.dgv_CANMessages.TabIndex = 12;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Width = 50;
            // 
            // DLC
            // 
            this.DLC.DataPropertyName = "DLC";
            this.DLC.HeaderText = "DLC";
            this.DLC.Name = "DLC";
            this.DLC.Width = 50;
            // 
            // RTR
            // 
            this.RTR.DataPropertyName = "RTR";
            this.RTR.HeaderText = "RTR";
            this.RTR.Name = "RTR";
            this.RTR.Width = 50;
            // 
            // D0
            // 
            this.D0.DataPropertyName = "D0";
            this.D0.HeaderText = "D0";
            this.D0.Name = "D0";
            this.D0.Width = 50;
            // 
            // D1
            // 
            this.D1.DataPropertyName = "D1";
            this.D1.HeaderText = "D1";
            this.D1.Name = "D1";
            this.D1.Width = 50;
            // 
            // D2
            // 
            this.D2.DataPropertyName = "D2";
            this.D2.HeaderText = "D2";
            this.D2.Name = "D2";
            this.D2.Width = 50;
            // 
            // D3
            // 
            this.D3.DataPropertyName = "D3";
            this.D3.HeaderText = "D3";
            this.D3.Name = "D3";
            this.D3.Width = 50;
            // 
            // D4
            // 
            this.D4.DataPropertyName = "D4";
            this.D4.HeaderText = "D4";
            this.D4.Name = "D4";
            this.D4.Width = 50;
            // 
            // D5
            // 
            this.D5.DataPropertyName = "D5";
            this.D5.HeaderText = "D5";
            this.D5.Name = "D5";
            this.D5.Width = 50;
            // 
            // D6
            // 
            this.D6.DataPropertyName = "D6";
            this.D6.HeaderText = "D6";
            this.D6.Name = "D6";
            this.D6.Width = 50;
            // 
            // D7
            // 
            this.D7.DataPropertyName = "D7";
            this.D7.HeaderText = "D7";
            this.D7.Name = "D7";
            this.D7.Width = 50;
            // 
            // Timestamp
            // 
            this.Timestamp.DataPropertyName = "Parameter";
            this.Timestamp.HeaderText = "Parameter";
            this.Timestamp.Name = "Timestamp";
            // 
            // description
            // 
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            // 
            // Form_FileAddEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 637);
            this.Controls.Add(this.dgv_CANMessages);
            this.Controls.Add(this.toolStripAddMessages);
            this.Controls.Add(this.toolStripMainFunc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_FileAddEdit";
            this.Text = "FileAddEdit";
            this.toolStripMainFunc.ResumeLayout(false);
            this.toolStripMainFunc.PerformLayout();
            this.toolStripAddMessages.ResumeLayout(false);
            this.toolStripAddMessages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANMessages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMainFunc;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tb_fileName;
        private System.Windows.Forms.ToolStripButton btn_Save;
        private System.Windows.Forms.ToolStrip toolStripAddMessages;
        private System.Windows.Forms.ToolStripButton btn_AddMessage;
        private System.Windows.Forms.ToolStripButton btn_DeleteMessage;
        private System.Windows.Forms.DataGridView dgv_CANMessages;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton btn_Open;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTR;
        private System.Windows.Forms.DataGridViewTextBoxColumn D0;
        private System.Windows.Forms.DataGridViewTextBoxColumn D1;
        private System.Windows.Forms.DataGridViewTextBoxColumn D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn D3;
        private System.Windows.Forms.DataGridViewTextBoxColumn D4;
        private System.Windows.Forms.DataGridViewTextBoxColumn D5;
        private System.Windows.Forms.DataGridViewTextBoxColumn D6;
        private System.Windows.Forms.DataGridViewTextBoxColumn D7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
    }
}