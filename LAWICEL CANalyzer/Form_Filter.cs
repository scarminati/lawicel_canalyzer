﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAWICEL_CANalyzer
{
    public partial class Form_Filter : Form
    {
        private List<Range<int>> listIdRangePass;
        private List<Range<int>> listIdRangeStop;
        public Form_Filter(List<Range<int>> listIdRangePass, List<Range<int>> listIdRangeStop)
        {
            InitializeComponent();
            this.listIdRangePass = listIdRangePass;
            this.listIdRangeStop = listIdRangeStop;
            setRangeLists();
        }

        private void setRangeLists()
        {
            try
            {
                foreach (Range<int> r in listIdRangeStop)
                {
                    dgv_stopRange.Rows.Clear();
                    dgv_stopRange.Rows.Insert(0, "", rb_Hex.Checked ? r.ShowRange("X") : r.ShowRange());
                }
                foreach (Range<int> r in listIdRangePass)
                {
                    dgv_passRange.Rows.Clear();
                    dgv_passRange.Rows.Insert(0, "", rb_Hex.Checked ? r.ShowRange("X") : r.ShowRange());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Rb_Dec_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_Dec.Checked)
                setRangeLists();
        }

        private void Rb_Hex_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_Hex.Checked)
                setRangeLists();
        }
        private void Btn_AddCondition_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_Condition.SelectedItem == null)
                    MessageBox.Show("Select a valid condition", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (tb_MinValue.Text.Equals(""))
                    MessageBox.Show("Select a valid minimum value for the range", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (tb_MaxValue.Text.Equals(""))
                    MessageBox.Show("Select a valid maximum value for the range", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Range<int> newRange = new Range<int>();
                    newRange.Minimum = Convert.ToInt32(Utility.replaceUselessChars(tb_MinValue.Text));
                    newRange.Maximum = Convert.ToInt32(Utility.replaceUselessChars(tb_MaxValue.Text));
                    switch (cmb_Condition.SelectedItem)
                    {
                        case "Pass":
                            {
                                        
                            }
                            break;
                        case "Fail":
                            {

                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void Btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_GoToStop_Click(object sender, EventArgs e)
        {

        }

        private void Btn_GoToPass_Click(object sender, EventArgs e)
        {

        }
    }
}
