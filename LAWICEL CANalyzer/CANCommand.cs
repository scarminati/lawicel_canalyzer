﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace LAWICEL_CANalyzer
{
    public class CANCommand : IEquatable<CANCommand>
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct SplitUlong
        {
            [FieldOffset(0)]
            public byte d0;
            [FieldOffset(1)]
            public byte d1;
            [FieldOffset(2)]
            public byte d2;
            [FieldOffset(3)]
            public byte d3;
            [FieldOffset(4)]
            public byte d4;
            [FieldOffset(5)]
            public byte d5;
            [FieldOffset(6)]
            public byte d6;
            [FieldOffset(7)]
            public byte d7;
            [FieldOffset(0)]
            public ulong data;
        }

        public enum MexType : int
        {
            Rx,Tx
        };

        private SplitUlong dataBytes;
        private int retCode=0;
        private uint id = 0x000;
        private byte flags = 0x00;
        private ulong dataULong;
        private byte dlc = 0;
        private byte[] data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        private string description;
        private uint timeStamp = 0;
        private int messageType = 0;//0 = Rx; 1 = Tx
        private int nextMessage = 0;
        private DateTime actDateTime;

        //costruttore di default
        public CANCommand() {}

        //overload costruttore 

        public CANCommand(uint id, byte dlc, byte flags, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7, string description = "", uint timeStamp = 0, int nextMessage = 0)
        {
            this.retCode = 0;
            ID = id;
            Flags = flags;
            DLC = dlc;
            byte[] bytes = { d0, d1, d2, d3, d4, d5, d6, d7 };
            DataULong = BitConverter.ToUInt64(bytes, 0);
            Description = description;
            TimeStamp = timeStamp;
        }

        //public uint ID { get { return this.id; } set { this.id = value; } }

        public int RetCode { get; set; }

        public uint ID { get; set; }

        public byte Flags { get; set; }

        public byte DLC { get; set; }

        public ulong DataULong { get { return this.dataULong; } set { this.dataULong = value; this.dataBytes.data = dataULong; DataBytes = dataBytes; } }

        public SplitUlong DataBytes { get; set; }

        public string Description { get; set; }

        public uint TimeStamp { get; set; }

        public int MessageType { get; set; }

        public DateTime ActDateTime { get; set; }

        public void cleanData()
        {
            if (DLC < 8)
                DataULong = DataULong & 0x00FFFFFFFFFFFFFF;
            if (DLC < 7)
                DataULong = DataULong & 0x0000FFFFFFFFFFFF;
            if (DLC < 6)
                DataULong = DataULong & 0x000000FFFFFFFFFF;
            if (DLC < 5)
                DataULong = DataULong & 0x00000000FFFFFFFF;
            if (DLC < 4)
                DataULong = DataULong & 0x0000000000FFFFFF;
            if (DLC < 3)
                DataULong = DataULong & 0x000000000000FFFF;
            if (DLC < 2)
                DataULong = DataULong & 0x00000000000000FF;
            if (DLC < 1)
                DataULong = DataULong & 0x0000000000000000;
        }

        public bool Equals(CANCommand cmd)
        {
            if (this.ID == cmd.ID && this.DLC == cmd.DLC && this.DataBytes.d0 == cmd.DataBytes.d0 && this.DataBytes.d1 == cmd.DataBytes.d1
                 && this.DataBytes.d2 == cmd.DataBytes.d2 && this.DataBytes.d3 == cmd.DataBytes.d3 && this.DataBytes.d4 == cmd.DataBytes.d4 && this.DataBytes.d5 == cmd.DataBytes.d5
                 && this.DataBytes.d6 == cmd.DataBytes.d6 && this.DataBytes.d7 == cmd.DataBytes.d7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
