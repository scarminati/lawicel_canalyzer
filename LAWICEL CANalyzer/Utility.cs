﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lawicel;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.Drawing;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using Newtonsoft.Json;
using System.Xml;

namespace LAWICEL_CANalyzer
{
    public static class Utility
    {
        public static string replaceUselessChars(string input)
        {
            string output = input;
            string pattern = "[a-fA-F0-9][a-fA-F0-9][a-fA-F0-9]";
            Match result = Regex.Match(input, pattern);
            if (!result.Success)
            {
                string patternReplace = "[a-fA-F0-9]";
                StringBuilder complementary = new StringBuilder(Regex.Replace(input, patternReplace, "@"));
                for (int i = 0; i < complementary.Length; i++)
                    if (complementary[i].Equals('@'))
                        complementary[i] = input[i];
                    else
                        complementary[i] = '0';
                output = complementary.ToString();
            }
            return output;
        }
        /*DESCRIZIONE : rileva i dispositivi CANusb connessi al PC
         INPUT : 
         OUTPUT : */
        public static List<String> getCanUSBList()
        {
            StringBuilder buf = new StringBuilder(32);
            List<String> devicesList = new List<String>();
            int i;
            int numCANusbConnectedToPC;
            numCANusbConnectedToPC = CANUSB.canusb_getFirstAdapter(buf, 32);
            if (numCANusbConnectedToPC != 0)
            {
                devicesList.Add(buf.ToString());
                for (i = 1; i < numCANusbConnectedToPC; i++)
                {
                    if (CANUSB.canusb_getNextAdapter(buf, 32) > 0)
                        devicesList.Add(buf.ToString());
                }
            }
            return (devicesList);
        }

        public static string hexValidator(string hexValue, int numChars)
        {
            string retValue = "";
            switch (hexValue.Length)
            {
                case 0:
                    for (int i = 0; i < numChars; i++)
                        retValue = retValue + "0";
                    break;
                default:
                    retValue = hexValue.PadLeft(numChars, '0');
                    break;
            }
            string pattern = "[^a-fA-F0-9]";
            retValue = Regex.Replace(retValue, pattern, "0");
            return retValue;
        }


        public static bool saveFile(DataGridView dgv, string fileName, string subFolderName, bool fileOverwrite)
        {
            try
            {
                List<String> dgvLines = new List<String>();
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    string res = "";
                    foreach (DataGridViewCell c in r.Cells)
                    {
                        switch (c.ColumnIndex)
                        {
                            case 0://ID
                                res = res + hexValidator(c.Value.ToString(), 3) + ";";
                                break;
                            case 1://DLC
                            case 2://RTR
                                res = res + hexValidator(c.Value.ToString(), 1) + ";";
                                break;
                            case 11://TIMESTAMP
                                res = res + hexValidator(c.Value.ToString(), 7) + ";";
                                break;
                            case 12://DESCRIPTION
                                res = res + (c.Value == null? "" : c.Value.ToString()) + ";";
                                break;
                            default:
                                res = res + hexValidator(c.Value.ToString(), 2) + ";";
                                break;
                        }
                    }
                    //rimuovo l'ultimo carattere, perchè superfluo.
                    res = res.Remove(res.Length - 1);
                    dgvLines.Insert(0,res);
                }
                string json = JsonConvert.SerializeObject(dgvLines);
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + subFolderName))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + subFolderName);
                if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + subFolderName + @"\" + fileName + ".txt"))
                    System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + subFolderName + @"\" + fileName + ".txt", json);
                else
                {
                    if (fileOverwrite)
                        System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + subFolderName + @"\" + fileName + ".txt", json);
                    else
                    {
                        //se il file esiste, non lo sovrascrivo, ma creo una copia numerata, che a sua volta non può essere sovrascritta.
                        int idx = 1;
                        while (true)
                        {
                            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + subFolderName + @"\" + fileName + " - Copy" + idx + ".txt"))
                            {
                                System.IO.File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + subFolderName + @"\" + fileName + " - Copy" + idx + ".txt", json);
                                break;
                            }
                            idx++;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<List<CANCommand>> loadCanCommandFile(List<String> scriptsFile, string subFolderName)
        {
            try
            {
                if (!scriptsFile.Count.Equals(0))
                {
                    List<List<CANCommand>> scripts = new List<List<CANCommand>>();
                    bool ok = true;
                    foreach (string s in scriptsFile)
                        if (!s.Contains(".txt"))
                        {
                            ok = false;
                            break;
                        }
                    if (ok.Equals(true))
                    {
                        foreach (string file in scriptsFile)
                        {
                            List<CANCommand> elenco = readCanCommandFile(Environment.CurrentDirectory + subFolderName + @"\" + file);
                            if (elenco.Count > 0)
                            {
                                scripts.Add(elenco);
                            }
                        }
                        return (scripts);
                    }
                    else
                    {
                        MessageBox.Show("Wrong script files selected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return null;
                    }
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public static List<CANCommand> readCanCommandFile(string filename)
        {
            List<CANCommand> elenco = new List<CANCommand>();
            List<String> canMessages = JsonConvert.DeserializeObject<List<String>>(File.ReadAllText(filename));
            foreach (string canMessage in canMessages)
            {
                string[] dataCanMessage = canMessage.Split(';');
                CANCommand canCmd = new CANCommand(Convert.ToUInt32(dataCanMessage[0], 16), Convert.ToByte(dataCanMessage[1], 16), Convert.ToByte(dataCanMessage[2], 16), Convert.ToByte(dataCanMessage[3], 16), Convert.ToByte(dataCanMessage[4], 16), Convert.ToByte(dataCanMessage[5], 16), Convert.ToByte(dataCanMessage[6], 16), Convert.ToByte(dataCanMessage[7], 16), Convert.ToByte(dataCanMessage[8], 16), Convert.ToByte(dataCanMessage[9], 16), Convert.ToByte(dataCanMessage[10], 16), dataCanMessage[12], Convert.ToUInt32(dataCanMessage[11], 10));
                elenco.Add(canCmd);
            }
            return elenco;
        }
    }
}
