﻿namespace LAWICEL_CANalyzer
{
    partial class FormMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panel_CANUSB_Lateral = new System.Windows.Forms.Panel();
            this.grp_Statistics = new System.Windows.Forms.GroupBox();
            this.lbl_RxRemoteFrames = new System.Windows.Forms.Label();
            this.lbl_RxFrames = new System.Windows.Forms.Label();
            this.lbl_TxRemoteFrames = new System.Windows.Forms.Label();
            this.lbl_TxFrames = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grp_controlPanel = new System.Windows.Forms.GroupBox();
            this.toolStrip12 = new System.Windows.Forms.ToolStrip();
            this.btn_send10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend10 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip11 = new System.Windows.Forms.ToolStrip();
            this.btn_send9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend9 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip10 = new System.Windows.Forms.ToolStrip();
            this.btn_send8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend8 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip9 = new System.Windows.Forms.ToolStrip();
            this.btn_send7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend7 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip8 = new System.Windows.Forms.ToolStrip();
            this.btn_send6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend6 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this.btn_send5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend5 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.btn_send4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend4 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.btn_send3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend3 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.btn_send2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btn_send1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tb_msgSend1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btn_selectCmdList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_createCmdList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_editCmdList = new System.Windows.Forms.ToolStripButton();
            this.panel_CANTrace = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssl_Time = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.tssl_Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel_Send = new System.Windows.Forms.Panel();
            this.grp_Scripts = new System.Windows.Forms.GroupBox();
            this.toolStrip13 = new System.Windows.Forms.ToolStrip();
            this.btn_selectScripts = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_createScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_editScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_startScripts = new System.Windows.Forms.ToolStripButton();
            this.grp_SendMessage = new System.Windows.Forms.GroupBox();
            this.tb_ID = new System.Windows.Forms.MaskedTextBox();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.cb_RTR = new System.Windows.Forms.CheckBox();
            this.tb_D7 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D6 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D5 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D4 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D3 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D2 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D1 = new System.Windows.Forms.MaskedTextBox();
            this.tb_D0 = new System.Windows.Forms.MaskedTextBox();
            this.tb_DLC = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Refresh = new System.Windows.Forms.ToolStripButton();
            this.cmb_BaudRate = new System.Windows.Forms.ToolStripComboBox();
            this.cmb_InterfaceList = new System.Windows.Forms.ToolStripComboBox();
            this.btn_OnOff = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Clean = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Filter = new System.Windows.Forms.ToolStripButton();
            this.btn_Highightning = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.timerVita = new System.Windows.Forms.Timer(this.components);
            this.dgv_CANTrace = new LAWICEL_CANalyzer.CustomDataGridView();
            this.dateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DLC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_CANUSB_Lateral.SuspendLayout();
            this.grp_Statistics.SuspendLayout();
            this.grp_controlPanel.SuspendLayout();
            this.toolStrip12.SuspendLayout();
            this.toolStrip11.SuspendLayout();
            this.toolStrip10.SuspendLayout();
            this.toolStrip9.SuspendLayout();
            this.toolStrip8.SuspendLayout();
            this.toolStrip7.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel_CANTrace.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel_Send.SuspendLayout();
            this.grp_Scripts.SuspendLayout();
            this.toolStrip13.SuspendLayout();
            this.grp_SendMessage.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_CANUSB_Lateral
            // 
            this.panel_CANUSB_Lateral.Controls.Add(this.grp_Statistics);
            this.panel_CANUSB_Lateral.Controls.Add(this.grp_controlPanel);
            this.panel_CANUSB_Lateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_CANUSB_Lateral.Location = new System.Drawing.Point(0, 0);
            this.panel_CANUSB_Lateral.Name = "panel_CANUSB_Lateral";
            this.panel_CANUSB_Lateral.Size = new System.Drawing.Size(222, 706);
            this.panel_CANUSB_Lateral.TabIndex = 9;
            // 
            // grp_Statistics
            // 
            this.grp_Statistics.Controls.Add(this.lbl_RxRemoteFrames);
            this.grp_Statistics.Controls.Add(this.lbl_RxFrames);
            this.grp_Statistics.Controls.Add(this.lbl_TxRemoteFrames);
            this.grp_Statistics.Controls.Add(this.lbl_TxFrames);
            this.grp_Statistics.Controls.Add(this.label13);
            this.grp_Statistics.Controls.Add(this.label14);
            this.grp_Statistics.Controls.Add(this.label12);
            this.grp_Statistics.Controls.Add(this.label3);
            this.grp_Statistics.Location = new System.Drawing.Point(6, 588);
            this.grp_Statistics.Name = "grp_Statistics";
            this.grp_Statistics.Size = new System.Drawing.Size(209, 114);
            this.grp_Statistics.TabIndex = 1;
            this.grp_Statistics.TabStop = false;
            this.grp_Statistics.Text = "Statistics";
            // 
            // lbl_RxRemoteFrames
            // 
            this.lbl_RxRemoteFrames.AutoSize = true;
            this.lbl_RxRemoteFrames.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_RxRemoteFrames.Location = new System.Drawing.Point(116, 93);
            this.lbl_RxRemoteFrames.MaximumSize = new System.Drawing.Size(80, 15);
            this.lbl_RxRemoteFrames.MinimumSize = new System.Drawing.Size(80, 15);
            this.lbl_RxRemoteFrames.Name = "lbl_RxRemoteFrames";
            this.lbl_RxRemoteFrames.Size = new System.Drawing.Size(80, 15);
            this.lbl_RxRemoteFrames.TabIndex = 7;
            // 
            // lbl_RxFrames
            // 
            this.lbl_RxFrames.AutoSize = true;
            this.lbl_RxFrames.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_RxFrames.Location = new System.Drawing.Point(116, 68);
            this.lbl_RxFrames.MaximumSize = new System.Drawing.Size(80, 15);
            this.lbl_RxFrames.MinimumSize = new System.Drawing.Size(80, 15);
            this.lbl_RxFrames.Name = "lbl_RxFrames";
            this.lbl_RxFrames.Size = new System.Drawing.Size(80, 15);
            this.lbl_RxFrames.TabIndex = 6;
            // 
            // lbl_TxRemoteFrames
            // 
            this.lbl_TxRemoteFrames.AutoSize = true;
            this.lbl_TxRemoteFrames.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_TxRemoteFrames.Location = new System.Drawing.Point(116, 43);
            this.lbl_TxRemoteFrames.MaximumSize = new System.Drawing.Size(80, 15);
            this.lbl_TxRemoteFrames.MinimumSize = new System.Drawing.Size(80, 15);
            this.lbl_TxRemoteFrames.Name = "lbl_TxRemoteFrames";
            this.lbl_TxRemoteFrames.Size = new System.Drawing.Size(80, 15);
            this.lbl_TxRemoteFrames.TabIndex = 5;
            // 
            // lbl_TxFrames
            // 
            this.lbl_TxFrames.AutoSize = true;
            this.lbl_TxFrames.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_TxFrames.Location = new System.Drawing.Point(116, 18);
            this.lbl_TxFrames.MaximumSize = new System.Drawing.Size(80, 15);
            this.lbl_TxFrames.MinimumSize = new System.Drawing.Size(80, 15);
            this.lbl_TxFrames.Name = "lbl_TxFrames";
            this.lbl_TxFrames.Size = new System.Drawing.Size(80, 15);
            this.lbl_TxFrames.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Rx Remote Frames";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Rx Frames";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Tx Remote Frames";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tx Frames";
            // 
            // grp_controlPanel
            // 
            this.grp_controlPanel.Controls.Add(this.toolStrip12);
            this.grp_controlPanel.Controls.Add(this.toolStrip11);
            this.grp_controlPanel.Controls.Add(this.toolStrip10);
            this.grp_controlPanel.Controls.Add(this.toolStrip9);
            this.grp_controlPanel.Controls.Add(this.toolStrip8);
            this.grp_controlPanel.Controls.Add(this.toolStrip7);
            this.grp_controlPanel.Controls.Add(this.toolStrip6);
            this.grp_controlPanel.Controls.Add(this.toolStrip5);
            this.grp_controlPanel.Controls.Add(this.toolStrip4);
            this.grp_controlPanel.Controls.Add(this.toolStrip3);
            this.grp_controlPanel.Controls.Add(this.toolStrip2);
            this.grp_controlPanel.Enabled = false;
            this.grp_controlPanel.Location = new System.Drawing.Point(6, 3);
            this.grp_controlPanel.Name = "grp_controlPanel";
            this.grp_controlPanel.Size = new System.Drawing.Size(209, 586);
            this.grp_controlPanel.TabIndex = 0;
            this.grp_controlPanel.TabStop = false;
            this.grp_controlPanel.Text = "Control Panel";
            // 
            // toolStrip12
            // 
            this.toolStrip12.AutoSize = false;
            this.toolStrip12.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip12.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip12.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send10,
            this.toolStripSeparator12,
            this.tb_msgSend10});
            this.toolStrip12.Location = new System.Drawing.Point(3, 516);
            this.toolStrip12.Name = "toolStrip12";
            this.toolStrip12.Size = new System.Drawing.Size(203, 50);
            this.toolStrip12.TabIndex = 19;
            this.toolStrip12.Text = "toolStrip12";
            // 
            // btn_send10
            // 
            this.btn_send10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send10.Image = ((System.Drawing.Image)(resources.GetObject("btn_send10.Image")));
            this.btn_send10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send10.Name = "btn_send10";
            this.btn_send10.Size = new System.Drawing.Size(51, 47);
            this.btn_send10.Text = "toolStripButton2";
            this.btn_send10.ToolTipText = "Refresh";
            this.btn_send10.Click += new System.EventHandler(this.Btn_send10_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend10
            // 
            this.tb_msgSend10.Name = "tb_msgSend10";
            this.tb_msgSend10.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip11
            // 
            this.toolStrip11.AutoSize = false;
            this.toolStrip11.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip11.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip11.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send9,
            this.toolStripSeparator11,
            this.tb_msgSend9});
            this.toolStrip11.Location = new System.Drawing.Point(3, 466);
            this.toolStrip11.Name = "toolStrip11";
            this.toolStrip11.Size = new System.Drawing.Size(203, 50);
            this.toolStrip11.TabIndex = 18;
            this.toolStrip11.Text = "toolStrip11";
            // 
            // btn_send9
            // 
            this.btn_send9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send9.Image = ((System.Drawing.Image)(resources.GetObject("btn_send9.Image")));
            this.btn_send9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send9.Name = "btn_send9";
            this.btn_send9.Size = new System.Drawing.Size(51, 47);
            this.btn_send9.Text = "toolStripButton2";
            this.btn_send9.ToolTipText = "Refresh";
            this.btn_send9.Click += new System.EventHandler(this.Btn_send9_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend9
            // 
            this.tb_msgSend9.Name = "tb_msgSend9";
            this.tb_msgSend9.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip10
            // 
            this.toolStrip10.AutoSize = false;
            this.toolStrip10.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip10.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip10.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send8,
            this.toolStripSeparator10,
            this.tb_msgSend8});
            this.toolStrip10.Location = new System.Drawing.Point(3, 416);
            this.toolStrip10.Name = "toolStrip10";
            this.toolStrip10.Size = new System.Drawing.Size(203, 50);
            this.toolStrip10.TabIndex = 17;
            this.toolStrip10.Text = "toolStrip10";
            // 
            // btn_send8
            // 
            this.btn_send8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send8.Image = ((System.Drawing.Image)(resources.GetObject("btn_send8.Image")));
            this.btn_send8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send8.Name = "btn_send8";
            this.btn_send8.Size = new System.Drawing.Size(51, 47);
            this.btn_send8.Text = "toolStripButton2";
            this.btn_send8.ToolTipText = "Refresh";
            this.btn_send8.Click += new System.EventHandler(this.Btn_send8_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend8
            // 
            this.tb_msgSend8.Name = "tb_msgSend8";
            this.tb_msgSend8.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip9
            // 
            this.toolStrip9.AutoSize = false;
            this.toolStrip9.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip9.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip9.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send7,
            this.toolStripSeparator9,
            this.tb_msgSend7});
            this.toolStrip9.Location = new System.Drawing.Point(3, 366);
            this.toolStrip9.Name = "toolStrip9";
            this.toolStrip9.Size = new System.Drawing.Size(203, 50);
            this.toolStrip9.TabIndex = 16;
            this.toolStrip9.Text = "toolStrip9";
            // 
            // btn_send7
            // 
            this.btn_send7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send7.Image = ((System.Drawing.Image)(resources.GetObject("btn_send7.Image")));
            this.btn_send7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send7.Name = "btn_send7";
            this.btn_send7.Size = new System.Drawing.Size(51, 47);
            this.btn_send7.Text = "toolStripButton2";
            this.btn_send7.ToolTipText = "Refresh";
            this.btn_send7.Click += new System.EventHandler(this.Btn_send7_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend7
            // 
            this.tb_msgSend7.Name = "tb_msgSend7";
            this.tb_msgSend7.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip8
            // 
            this.toolStrip8.AutoSize = false;
            this.toolStrip8.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip8.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send6,
            this.toolStripSeparator8,
            this.tb_msgSend6});
            this.toolStrip8.Location = new System.Drawing.Point(3, 316);
            this.toolStrip8.Name = "toolStrip8";
            this.toolStrip8.Size = new System.Drawing.Size(203, 50);
            this.toolStrip8.TabIndex = 15;
            this.toolStrip8.Text = "toolStrip8";
            // 
            // btn_send6
            // 
            this.btn_send6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send6.Image = ((System.Drawing.Image)(resources.GetObject("btn_send6.Image")));
            this.btn_send6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send6.Name = "btn_send6";
            this.btn_send6.Size = new System.Drawing.Size(51, 47);
            this.btn_send6.Text = "toolStripButton2";
            this.btn_send6.ToolTipText = "Refresh";
            this.btn_send6.Click += new System.EventHandler(this.Btn_send6_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend6
            // 
            this.tb_msgSend6.Name = "tb_msgSend6";
            this.tb_msgSend6.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip7
            // 
            this.toolStrip7.AutoSize = false;
            this.toolStrip7.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip7.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send5,
            this.toolStripSeparator7,
            this.tb_msgSend5});
            this.toolStrip7.Location = new System.Drawing.Point(3, 266);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.Size = new System.Drawing.Size(203, 50);
            this.toolStrip7.TabIndex = 14;
            this.toolStrip7.Text = "toolStrip7";
            // 
            // btn_send5
            // 
            this.btn_send5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send5.Image = ((System.Drawing.Image)(resources.GetObject("btn_send5.Image")));
            this.btn_send5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send5.Name = "btn_send5";
            this.btn_send5.Size = new System.Drawing.Size(51, 47);
            this.btn_send5.Text = "toolStripButton2";
            this.btn_send5.ToolTipText = "Refresh";
            this.btn_send5.Click += new System.EventHandler(this.Btn_send5_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend5
            // 
            this.tb_msgSend5.Name = "tb_msgSend5";
            this.tb_msgSend5.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip6
            // 
            this.toolStrip6.AutoSize = false;
            this.toolStrip6.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip6.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send4,
            this.toolStripSeparator6,
            this.tb_msgSend4});
            this.toolStrip6.Location = new System.Drawing.Point(3, 216);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(203, 50);
            this.toolStrip6.TabIndex = 13;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // btn_send4
            // 
            this.btn_send4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send4.Image = ((System.Drawing.Image)(resources.GetObject("btn_send4.Image")));
            this.btn_send4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send4.Name = "btn_send4";
            this.btn_send4.Size = new System.Drawing.Size(51, 47);
            this.btn_send4.Text = "toolStripButton2";
            this.btn_send4.ToolTipText = "Refresh";
            this.btn_send4.Click += new System.EventHandler(this.Btn_send4_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend4
            // 
            this.tb_msgSend4.Name = "tb_msgSend4";
            this.tb_msgSend4.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip5
            // 
            this.toolStrip5.AutoSize = false;
            this.toolStrip5.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip5.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send3,
            this.toolStripSeparator5,
            this.tb_msgSend3});
            this.toolStrip5.Location = new System.Drawing.Point(3, 166);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(203, 50);
            this.toolStrip5.TabIndex = 12;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // btn_send3
            // 
            this.btn_send3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send3.Image = ((System.Drawing.Image)(resources.GetObject("btn_send3.Image")));
            this.btn_send3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send3.Name = "btn_send3";
            this.btn_send3.Size = new System.Drawing.Size(51, 47);
            this.btn_send3.Text = "toolStripButton2";
            this.btn_send3.ToolTipText = "Refresh";
            this.btn_send3.Click += new System.EventHandler(this.Btn_send3_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend3
            // 
            this.tb_msgSend3.Name = "tb_msgSend3";
            this.tb_msgSend3.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip4
            // 
            this.toolStrip4.AutoSize = false;
            this.toolStrip4.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send2,
            this.toolStripSeparator4,
            this.tb_msgSend2});
            this.toolStrip4.Location = new System.Drawing.Point(3, 116);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(203, 50);
            this.toolStrip4.TabIndex = 11;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // btn_send2
            // 
            this.btn_send2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send2.Image = ((System.Drawing.Image)(resources.GetObject("btn_send2.Image")));
            this.btn_send2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send2.Name = "btn_send2";
            this.btn_send2.Size = new System.Drawing.Size(51, 47);
            this.btn_send2.Text = "toolStripButton2";
            this.btn_send2.ToolTipText = "Refresh";
            this.btn_send2.Click += new System.EventHandler(this.Btn_send2_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend2
            // 
            this.tb_msgSend2.Name = "tb_msgSend2";
            this.tb_msgSend2.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip3
            // 
            this.toolStrip3.AutoSize = false;
            this.toolStrip3.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_send1,
            this.toolStripSeparator3,
            this.tb_msgSend1});
            this.toolStrip3.Location = new System.Drawing.Point(3, 66);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(203, 50);
            this.toolStrip3.TabIndex = 10;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btn_send1
            // 
            this.btn_send1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_send1.Image = ((System.Drawing.Image)(resources.GetObject("btn_send1.Image")));
            this.btn_send1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_send1.Name = "btn_send1";
            this.btn_send1.Size = new System.Drawing.Size(51, 47);
            this.btn_send1.Text = "toolStripButton2";
            this.btn_send1.ToolTipText = "Refresh";
            this.btn_send1.Click += new System.EventHandler(this.Btn_send1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            // 
            // tb_msgSend1
            // 
            this.tb_msgSend1.Name = "tb_msgSend1";
            this.tb_msgSend1.Size = new System.Drawing.Size(120, 50);
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_selectCmdList,
            this.toolStripSeparator16,
            this.btn_createCmdList,
            this.toolStripSeparator1,
            this.btn_editCmdList});
            this.toolStrip2.Location = new System.Drawing.Point(3, 16);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(203, 50);
            this.toolStrip2.TabIndex = 9;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btn_selectCmdList
            // 
            this.btn_selectCmdList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_selectCmdList.Image = ((System.Drawing.Image)(resources.GetObject("btn_selectCmdList.Image")));
            this.btn_selectCmdList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_selectCmdList.Name = "btn_selectCmdList";
            this.btn_selectCmdList.Size = new System.Drawing.Size(51, 47);
            this.btn_selectCmdList.Text = "toolStripButton2";
            this.btn_selectCmdList.ToolTipText = "Open Command List";
            this.btn_selectCmdList.Click += new System.EventHandler(this.btn_selectCmdList_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_createCmdList
            // 
            this.btn_createCmdList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_createCmdList.Image = ((System.Drawing.Image)(resources.GetObject("btn_createCmdList.Image")));
            this.btn_createCmdList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_createCmdList.Name = "btn_createCmdList";
            this.btn_createCmdList.Size = new System.Drawing.Size(51, 47);
            this.btn_createCmdList.Text = "toolStripButton2";
            this.btn_createCmdList.ToolTipText = "Add Command List";
            this.btn_createCmdList.Click += new System.EventHandler(this.btn_createCmdList_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_editCmdList
            // 
            this.btn_editCmdList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_editCmdList.Image = ((System.Drawing.Image)(resources.GetObject("btn_editCmdList.Image")));
            this.btn_editCmdList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_editCmdList.Name = "btn_editCmdList";
            this.btn_editCmdList.Size = new System.Drawing.Size(51, 47);
            this.btn_editCmdList.Text = "toolStripButton1";
            this.btn_editCmdList.ToolTipText = "Edit Command List";
            this.btn_editCmdList.Click += new System.EventHandler(this.btn_editCmdList_Click);
            // 
            // panel_CANTrace
            // 
            this.panel_CANTrace.Controls.Add(this.panel1);
            this.panel_CANTrace.Controls.Add(this.toolStrip1);
            this.panel_CANTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_CANTrace.Location = new System.Drawing.Point(222, 0);
            this.panel_CANTrace.Name = "panel_CANTrace";
            this.panel_CANTrace.Size = new System.Drawing.Size(874, 706);
            this.panel_CANTrace.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this.panel_Send);
            this.panel1.Controls.Add(this.dgv_CANTrace);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(874, 656);
            this.panel1.TabIndex = 9;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssl_Time,
            this.toolStripSeparator18,
            this.tssl_Status});
            this.statusStrip1.Location = new System.Drawing.Point(0, 633);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(874, 23);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssl_Time
            // 
            this.tssl_Time.Name = "tssl_Time";
            this.tssl_Time.Size = new System.Drawing.Size(34, 18);
            this.tssl_Time.Text = "Time";
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 23);
            // 
            // tssl_Status
            // 
            this.tssl_Status.Name = "tssl_Status";
            this.tssl_Status.Size = new System.Drawing.Size(39, 18);
            this.tssl_Status.Text = "Status";
            // 
            // panel_Send
            // 
            this.panel_Send.Controls.Add(this.grp_Scripts);
            this.panel_Send.Controls.Add(this.grp_SendMessage);
            this.panel_Send.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Send.Location = new System.Drawing.Point(0, 539);
            this.panel_Send.Name = "panel_Send";
            this.panel_Send.Size = new System.Drawing.Size(874, 117);
            this.panel_Send.TabIndex = 0;
            // 
            // grp_Scripts
            // 
            this.grp_Scripts.Controls.Add(this.toolStrip13);
            this.grp_Scripts.Enabled = false;
            this.grp_Scripts.Location = new System.Drawing.Point(608, 14);
            this.grp_Scripts.Name = "grp_Scripts";
            this.grp_Scripts.Size = new System.Drawing.Size(257, 76);
            this.grp_Scripts.TabIndex = 1;
            this.grp_Scripts.TabStop = false;
            this.grp_Scripts.Text = "Scripts";
            // 
            // toolStrip13
            // 
            this.toolStrip13.AutoSize = false;
            this.toolStrip13.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip13.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip13.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip13.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_selectScripts,
            this.toolStripSeparator14,
            this.btn_createScript,
            this.toolStripSeparator15,
            this.btn_editScript,
            this.toolStripSeparator13,
            this.btn_startScripts});
            this.toolStrip13.Location = new System.Drawing.Point(3, 16);
            this.toolStrip13.Name = "toolStrip13";
            this.toolStrip13.Size = new System.Drawing.Size(251, 50);
            this.toolStrip13.TabIndex = 10;
            this.toolStrip13.Text = "toolStrip13";
            // 
            // btn_selectScripts
            // 
            this.btn_selectScripts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_selectScripts.Image = ((System.Drawing.Image)(resources.GetObject("btn_selectScripts.Image")));
            this.btn_selectScripts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_selectScripts.Name = "btn_selectScripts";
            this.btn_selectScripts.Size = new System.Drawing.Size(51, 47);
            this.btn_selectScripts.Text = "toolStripButton2";
            this.btn_selectScripts.ToolTipText = "Open Script";
            this.btn_selectScripts.Click += new System.EventHandler(this.btn_selectScripts_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_createScript
            // 
            this.btn_createScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_createScript.Image = ((System.Drawing.Image)(resources.GetObject("btn_createScript.Image")));
            this.btn_createScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_createScript.Name = "btn_createScript";
            this.btn_createScript.Size = new System.Drawing.Size(51, 47);
            this.btn_createScript.Text = "toolStripButton5";
            this.btn_createScript.ToolTipText = "Add Script";
            this.btn_createScript.Click += new System.EventHandler(this.btn_createScript_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_editScript
            // 
            this.btn_editScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_editScript.Image = ((System.Drawing.Image)(resources.GetObject("btn_editScript.Image")));
            this.btn_editScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_editScript.Name = "btn_editScript";
            this.btn_editScript.Size = new System.Drawing.Size(51, 47);
            this.btn_editScript.Text = "toolStripButton2";
            this.btn_editScript.ToolTipText = "Edit Script";
            this.btn_editScript.Click += new System.EventHandler(this.btn_editScript_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_startScripts
            // 
            this.btn_startScripts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_startScripts.Image = ((System.Drawing.Image)(resources.GetObject("btn_startScripts.Image")));
            this.btn_startScripts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_startScripts.Name = "btn_startScripts";
            this.btn_startScripts.Size = new System.Drawing.Size(51, 47);
            this.btn_startScripts.Tag = "Start";
            this.btn_startScripts.Text = "toolStripButton1";
            this.btn_startScripts.ToolTipText = "Start";
            this.btn_startScripts.Click += new System.EventHandler(this.btn_startScripts_Click);
            // 
            // grp_SendMessage
            // 
            this.grp_SendMessage.Controls.Add(this.tb_ID);
            this.grp_SendMessage.Controls.Add(this.btn_Submit);
            this.grp_SendMessage.Controls.Add(this.cb_RTR);
            this.grp_SendMessage.Controls.Add(this.tb_D7);
            this.grp_SendMessage.Controls.Add(this.tb_D6);
            this.grp_SendMessage.Controls.Add(this.tb_D5);
            this.grp_SendMessage.Controls.Add(this.tb_D4);
            this.grp_SendMessage.Controls.Add(this.tb_D3);
            this.grp_SendMessage.Controls.Add(this.tb_D2);
            this.grp_SendMessage.Controls.Add(this.tb_D1);
            this.grp_SendMessage.Controls.Add(this.tb_D0);
            this.grp_SendMessage.Controls.Add(this.tb_DLC);
            this.grp_SendMessage.Controls.Add(this.label11);
            this.grp_SendMessage.Controls.Add(this.label10);
            this.grp_SendMessage.Controls.Add(this.label9);
            this.grp_SendMessage.Controls.Add(this.label8);
            this.grp_SendMessage.Controls.Add(this.label7);
            this.grp_SendMessage.Controls.Add(this.label6);
            this.grp_SendMessage.Controls.Add(this.label5);
            this.grp_SendMessage.Controls.Add(this.label4);
            this.grp_SendMessage.Controls.Add(this.label2);
            this.grp_SendMessage.Controls.Add(this.label1);
            this.grp_SendMessage.Enabled = false;
            this.grp_SendMessage.Location = new System.Drawing.Point(6, 14);
            this.grp_SendMessage.Name = "grp_SendMessage";
            this.grp_SendMessage.Size = new System.Drawing.Size(596, 76);
            this.grp_SendMessage.TabIndex = 0;
            this.grp_SendMessage.TabStop = false;
            this.grp_SendMessage.Text = "Send Message";
            // 
            // tb_ID
            // 
            this.tb_ID.Location = new System.Drawing.Point(18, 36);
            this.tb_ID.Mask = "&&&";
            this.tb_ID.Name = "tb_ID";
            this.tb_ID.Size = new System.Drawing.Size(28, 20);
            this.tb_ID.TabIndex = 2;
            this.tb_ID.Enter += new System.EventHandler(this.tb_ID_Enter);
            // 
            // btn_Submit
            // 
            this.btn_Submit.ImageKey = "submit.png";
            this.btn_Submit.ImageList = this.imageList;
            this.btn_Submit.Location = new System.Drawing.Point(528, 14);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 13;
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.btn_Submit_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            this.imageList.Images.SetKeyName(10, "rubber.png");
            this.imageList.Images.SetKeyName(11, "filter.png");
            this.imageList.Images.SetKeyName(12, "statistics.jpg");
            // 
            // cb_RTR
            // 
            this.cb_RTR.AutoSize = true;
            this.cb_RTR.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cb_RTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_RTR.Location = new System.Drawing.Point(110, 19);
            this.cb_RTR.Name = "cb_RTR";
            this.cb_RTR.Size = new System.Drawing.Size(37, 31);
            this.cb_RTR.TabIndex = 4;
            this.cb_RTR.Text = "RTR\r\n";
            this.cb_RTR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb_RTR.UseVisualStyleBackColor = true;
            // 
            // tb_D7
            // 
            this.tb_D7.Location = new System.Drawing.Point(485, 36);
            this.tb_D7.Mask = "&&";
            this.tb_D7.Name = "tb_D7";
            this.tb_D7.Size = new System.Drawing.Size(20, 20);
            this.tb_D7.TabIndex = 12;
            this.tb_D7.Enter += new System.EventHandler(this.tb_D7_Enter);
            // 
            // tb_D6
            // 
            this.tb_D6.Location = new System.Drawing.Point(442, 36);
            this.tb_D6.Mask = "&&";
            this.tb_D6.Name = "tb_D6";
            this.tb_D6.Size = new System.Drawing.Size(20, 20);
            this.tb_D6.TabIndex = 11;
            this.tb_D6.Enter += new System.EventHandler(this.tb_D6_Enter);
            // 
            // tb_D5
            // 
            this.tb_D5.Location = new System.Drawing.Point(393, 36);
            this.tb_D5.Mask = "&&";
            this.tb_D5.Name = "tb_D5";
            this.tb_D5.Size = new System.Drawing.Size(20, 20);
            this.tb_D5.TabIndex = 10;
            this.tb_D5.Enter += new System.EventHandler(this.tb_D5_Enter);
            // 
            // tb_D4
            // 
            this.tb_D4.Location = new System.Drawing.Point(347, 36);
            this.tb_D4.Mask = "&&";
            this.tb_D4.Name = "tb_D4";
            this.tb_D4.Size = new System.Drawing.Size(20, 20);
            this.tb_D4.TabIndex = 9;
            this.tb_D4.Enter += new System.EventHandler(this.tb_D4_Enter);
            // 
            // tb_D3
            // 
            this.tb_D3.Location = new System.Drawing.Point(301, 36);
            this.tb_D3.Mask = "&&";
            this.tb_D3.Name = "tb_D3";
            this.tb_D3.Size = new System.Drawing.Size(20, 20);
            this.tb_D3.TabIndex = 8;
            this.tb_D3.Enter += new System.EventHandler(this.tb_D3_Enter);
            // 
            // tb_D2
            // 
            this.tb_D2.Location = new System.Drawing.Point(255, 36);
            this.tb_D2.Mask = "&&";
            this.tb_D2.Name = "tb_D2";
            this.tb_D2.Size = new System.Drawing.Size(20, 20);
            this.tb_D2.TabIndex = 7;
            this.tb_D2.Enter += new System.EventHandler(this.tb_D2_Enter);
            // 
            // tb_D1
            // 
            this.tb_D1.Location = new System.Drawing.Point(209, 36);
            this.tb_D1.Mask = "&&";
            this.tb_D1.Name = "tb_D1";
            this.tb_D1.Size = new System.Drawing.Size(20, 20);
            this.tb_D1.TabIndex = 6;
            this.tb_D1.Enter += new System.EventHandler(this.tb_D1_Enter);
            // 
            // tb_D0
            // 
            this.tb_D0.Location = new System.Drawing.Point(163, 36);
            this.tb_D0.Mask = "&&";
            this.tb_D0.Name = "tb_D0";
            this.tb_D0.Size = new System.Drawing.Size(20, 20);
            this.tb_D0.TabIndex = 5;
            this.tb_D0.Enter += new System.EventHandler(this.tb_D0_Enter);
            // 
            // tb_DLC
            // 
            this.tb_DLC.Location = new System.Drawing.Point(72, 36);
            this.tb_DLC.Mask = "0";
            this.tb_DLC.Name = "tb_DLC";
            this.tb_DLC.Size = new System.Drawing.Size(20, 20);
            this.tb_DLC.TabIndex = 3;
            this.tb_DLC.Enter += new System.EventHandler(this.tb_DLC_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(482, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "D7";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(439, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "D6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(390, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "D5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(344, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "D4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(299, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "D3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(253, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "D2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(207, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "D1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "D0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(66, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "DLC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Refresh,
            this.cmb_BaudRate,
            this.cmb_InterfaceList,
            this.btn_OnOff,
            this.toolStripSeparator2,
            this.btn_Clean,
            this.toolStripSeparator17,
            this.btn_Filter,
            this.btn_Highightning,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(874, 50);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Refresh.Image = global::LAWICEL_CANalyzer.Properties.Resources.refreshIcon;
            this.btn_Refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(51, 47);
            this.btn_Refresh.Text = "toolStripButton2";
            this.btn_Refresh.ToolTipText = "Refresh";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // cmb_BaudRate
            // 
            this.cmb_BaudRate.BackColor = System.Drawing.SystemColors.Window;
            this.cmb_BaudRate.Items.AddRange(new object[] {
            "1000",
            "800",
            "500",
            "250",
            "125",
            "100",
            "50"});
            this.cmb_BaudRate.Name = "cmb_BaudRate";
            this.cmb_BaudRate.Size = new System.Drawing.Size(121, 50);
            this.cmb_BaudRate.ToolTipText = "Baud Rate [kHz]";
            // 
            // cmb_InterfaceList
            // 
            this.cmb_InterfaceList.BackColor = System.Drawing.Color.Salmon;
            this.cmb_InterfaceList.Name = "cmb_InterfaceList";
            this.cmb_InterfaceList.Size = new System.Drawing.Size(121, 50);
            this.cmb_InterfaceList.ToolTipText = "Interface Name";
            // 
            // btn_OnOff
            // 
            this.btn_OnOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_OnOff.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_OnOff.Image = global::LAWICEL_CANalyzer.Properties.Resources.onIcon;
            this.btn_OnOff.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_OnOff.Name = "btn_OnOff";
            this.btn_OnOff.Size = new System.Drawing.Size(51, 47);
            this.btn_OnOff.Tag = "On";
            this.btn_OnOff.ToolTipText = "Connect";
            this.btn_OnOff.Click += new System.EventHandler(this.btn_OnOff_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Clean
            // 
            this.btn_Clean.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Clean.Image = global::LAWICEL_CANalyzer.Properties.Resources.rubber;
            this.btn_Clean.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Clean.Name = "btn_Clean";
            this.btn_Clean.Size = new System.Drawing.Size(51, 47);
            this.btn_Clean.Text = "toolStripButton1";
            this.btn_Clean.ToolTipText = "Clean";
            this.btn_Clean.Click += new System.EventHandler(this.btn_Clean_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Filter
            // 
            this.btn_Filter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Filter.Image = global::LAWICEL_CANalyzer.Properties.Resources.filterIcon;
            this.btn_Filter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Filter.Name = "btn_Filter";
            this.btn_Filter.Size = new System.Drawing.Size(51, 47);
            this.btn_Filter.Text = "toolStripButton2";
            this.btn_Filter.ToolTipText = "Filter";
            this.btn_Filter.Click += new System.EventHandler(this.btn_Filter_Click);
            // 
            // btn_Highightning
            // 
            this.btn_Highightning.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Highightning.Image = ((System.Drawing.Image)(resources.GetObject("btn_Highightning.Image")));
            this.btn_Highightning.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Highightning.Name = "btn_Highightning";
            this.btn_Highightning.Size = new System.Drawing.Size(51, 47);
            this.btn_Highightning.Text = "toolStripButton1";
            this.btn_Highightning.ToolTipText = "Highlightning";
            this.btn_Highightning.Click += new System.EventHandler(this.Btn_Highightning_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton1.Text = "Statistics";
            // 
            // timerVita
            // 
            this.timerVita.Tick += new System.EventHandler(this.TimerVita_Tick);
            // 
            // dgv_CANTrace
            // 
            this.dgv_CANTrace.AllowUserToAddRows = false;
            this.dgv_CANTrace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_CANTrace.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateTime,
            this.dataGridViewTextBoxColumn1,
            this.ID,
            this.DLC,
            this.RTR,
            this.D0,
            this.D1,
            this.D2,
            this.D3,
            this.D4,
            this.D5,
            this.D6,
            this.D7,
            this.Timestamp});
            this.dgv_CANTrace.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgv_CANTrace.Location = new System.Drawing.Point(0, 0);
            this.dgv_CANTrace.Name = "dgv_CANTrace";
            this.dgv_CANTrace.ReadOnly = true;
            this.dgv_CANTrace.Size = new System.Drawing.Size(874, 539);
            this.dgv_CANTrace.TabIndex = 1;
            // 
            // dateTime
            // 
            this.dateTime.HeaderText = "DateTime";
            this.dateTime.Name = "dateTime";
            this.dateTime.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TxRx";
            this.dataGridViewTextBoxColumn1.HeaderText = "TxRx";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 50;
            // 
            // DLC
            // 
            this.DLC.DataPropertyName = "DLC";
            this.DLC.HeaderText = "DLC";
            this.DLC.Name = "DLC";
            this.DLC.ReadOnly = true;
            this.DLC.Width = 50;
            // 
            // RTR
            // 
            this.RTR.DataPropertyName = "RTR";
            this.RTR.HeaderText = "RTR";
            this.RTR.Name = "RTR";
            this.RTR.ReadOnly = true;
            this.RTR.Width = 50;
            // 
            // D0
            // 
            this.D0.DataPropertyName = "D0";
            this.D0.HeaderText = "D0";
            this.D0.Name = "D0";
            this.D0.ReadOnly = true;
            this.D0.Width = 50;
            // 
            // D1
            // 
            this.D1.DataPropertyName = "D1";
            this.D1.HeaderText = "D1";
            this.D1.Name = "D1";
            this.D1.ReadOnly = true;
            this.D1.Width = 50;
            // 
            // D2
            // 
            this.D2.DataPropertyName = "D2";
            this.D2.HeaderText = "D2";
            this.D2.Name = "D2";
            this.D2.ReadOnly = true;
            this.D2.Width = 50;
            // 
            // D3
            // 
            this.D3.DataPropertyName = "D3";
            this.D3.HeaderText = "D3";
            this.D3.Name = "D3";
            this.D3.ReadOnly = true;
            this.D3.Width = 50;
            // 
            // D4
            // 
            this.D4.DataPropertyName = "D4";
            this.D4.HeaderText = "D4";
            this.D4.Name = "D4";
            this.D4.ReadOnly = true;
            this.D4.Width = 50;
            // 
            // D5
            // 
            this.D5.DataPropertyName = "D5";
            this.D5.HeaderText = "D5";
            this.D5.Name = "D5";
            this.D5.ReadOnly = true;
            this.D5.Width = 50;
            // 
            // D6
            // 
            this.D6.DataPropertyName = "D6";
            this.D6.HeaderText = "D6";
            this.D6.Name = "D6";
            this.D6.ReadOnly = true;
            this.D6.Width = 50;
            // 
            // D7
            // 
            this.D7.DataPropertyName = "D7";
            this.D7.HeaderText = "D7";
            this.D7.Name = "D7";
            this.D7.ReadOnly = true;
            this.D7.Width = 50;
            // 
            // Timestamp
            // 
            this.Timestamp.DataPropertyName = "Timestamp";
            this.Timestamp.HeaderText = "Timestamp";
            this.Timestamp.Name = "Timestamp";
            this.Timestamp.ReadOnly = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 706);
            this.Controls.Add(this.panel_CANTrace);
            this.Controls.Add(this.panel_CANUSB_Lateral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1112, 744);
            this.MinimumSize = new System.Drawing.Size(1112, 726);
            this.Name = "FormMain";
            this.Text = "LAWICEL CANalyzer";
            this.panel_CANUSB_Lateral.ResumeLayout(false);
            this.grp_Statistics.ResumeLayout(false);
            this.grp_Statistics.PerformLayout();
            this.grp_controlPanel.ResumeLayout(false);
            this.toolStrip12.ResumeLayout(false);
            this.toolStrip12.PerformLayout();
            this.toolStrip11.ResumeLayout(false);
            this.toolStrip11.PerformLayout();
            this.toolStrip10.ResumeLayout(false);
            this.toolStrip10.PerformLayout();
            this.toolStrip9.ResumeLayout(false);
            this.toolStrip9.PerformLayout();
            this.toolStrip8.ResumeLayout(false);
            this.toolStrip8.PerformLayout();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel_CANTrace.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel_Send.ResumeLayout(false);
            this.grp_Scripts.ResumeLayout(false);
            this.toolStrip13.ResumeLayout(false);
            this.toolStrip13.PerformLayout();
            this.grp_SendMessage.ResumeLayout(false);
            this.grp_SendMessage.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_CANTrace)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel_CANUSB_Lateral;
        private System.Windows.Forms.Panel panel_CANTrace;
        private System.Windows.Forms.Panel panel1;
        private LAWICEL_CANalyzer.CustomDataGridView dgv_CANTrace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DLC;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTR;
        private System.Windows.Forms.DataGridViewTextBoxColumn D0;
        private System.Windows.Forms.DataGridViewTextBoxColumn D1;
        private System.Windows.Forms.DataGridViewTextBoxColumn D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn D3;
        private System.Windows.Forms.DataGridViewTextBoxColumn D4;
        private System.Windows.Forms.DataGridViewTextBoxColumn D5;
        private System.Windows.Forms.DataGridViewTextBoxColumn D6;
        private System.Windows.Forms.DataGridViewTextBoxColumn D7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
        private System.Windows.Forms.Panel panel_Send;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_Refresh;
        private System.Windows.Forms.ToolStripComboBox cmb_BaudRate;
        private System.Windows.Forms.ToolStripComboBox cmb_InterfaceList;
        private System.Windows.Forms.ToolStripButton btn_OnOff;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.GroupBox grp_SendMessage;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grp_controlPanel;
        private System.Windows.Forms.CheckBox cb_RTR;
        private System.Windows.Forms.MaskedTextBox tb_D7;
        private System.Windows.Forms.MaskedTextBox tb_D6;
        private System.Windows.Forms.MaskedTextBox tb_D5;
        private System.Windows.Forms.MaskedTextBox tb_D4;
        private System.Windows.Forms.MaskedTextBox tb_D3;
        private System.Windows.Forms.MaskedTextBox tb_D2;
        private System.Windows.Forms.MaskedTextBox tb_D1;
        private System.Windows.Forms.MaskedTextBox tb_D0;
        private System.Windows.Forms.MaskedTextBox tb_DLC;
        private System.Windows.Forms.MaskedTextBox tb_ID;
        private System.Windows.Forms.GroupBox grp_Scripts;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton btn_Clean;
        private System.Windows.Forms.ToolStripButton btn_Filter;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btn_selectCmdList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_editCmdList;
        private System.Windows.Forms.ToolStrip toolStrip12;
        private System.Windows.Forms.ToolStripButton btn_send10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend10;
        private System.Windows.Forms.ToolStrip toolStrip11;
        private System.Windows.Forms.ToolStripButton btn_send9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend9;
        private System.Windows.Forms.ToolStrip toolStrip10;
        private System.Windows.Forms.ToolStripButton btn_send8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend8;
        private System.Windows.Forms.ToolStrip toolStrip9;
        private System.Windows.Forms.ToolStripButton btn_send7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend7;
        private System.Windows.Forms.ToolStrip toolStrip8;
        private System.Windows.Forms.ToolStripButton btn_send6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend6;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripButton btn_send5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend5;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton btn_send4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend4;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton btn_send3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend3;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton btn_send2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend2;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btn_send1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripTextBox tb_msgSend1;
        private System.Windows.Forms.ToolStrip toolStrip13;
        private System.Windows.Forms.ToolStripButton btn_createScript;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton btn_selectScripts;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton btn_startScripts;
        private System.Windows.Forms.ToolStripButton btn_editScript;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton btn_createCmdList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripButton btn_Highightning;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssl_Status;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripStatusLabel tssl_Time;
        private System.Windows.Forms.Timer timerVita;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.GroupBox grp_Statistics;
        private System.Windows.Forms.Label lbl_RxRemoteFrames;
        private System.Windows.Forms.Label lbl_RxFrames;
        private System.Windows.Forms.Label lbl_TxRemoteFrames;
        private System.Windows.Forms.Label lbl_TxFrames;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
    }
}

