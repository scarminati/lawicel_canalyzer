﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LAWICEL_CANalyzer
{
    public partial class Form_Script : Form
    {
        public Form_Script(string formTitle)
        {
            try
            {
                InitializeComponent();
                setForm(formTitle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setForm(string formTitle)
        {
            try
            {
                this.Text = formTitle;
                switch (formTitle)
                {
                    case "Create Script":
                        btn_OpenScript.Visible = false;
                        break;
                    case "Edit Script":
                        btn_OpenScript.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddMessage_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_AddMessage())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        dgv_CANTrace.Rows.Insert(0, form.msg.ID.ToString("X3"), form.msg.DLC, form.msg.Flags, form.msg.DataBytes.d0.ToString("X2"), form.msg.DataBytes.d1.ToString("X2"), form.msg.DataBytes.d2.ToString("X2"), form.msg.DataBytes.d3.ToString("X2"), form.msg.DataBytes.d4.ToString("X2"), form.msg.DataBytes.d5.ToString("X2"), form.msg.DataBytes.d6.ToString("X2"), form.msg.DataBytes.d7.ToString("X2"), form.msg.TimeStamp);
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteMessage_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_CANTrace.Rows.Remove(dgv_CANTrace.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_OpenScript_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.InitialDirectory = Environment.CurrentDirectory + @"\Scripts";
                openFileDialog.Filter = "Script File (*.txt)|*.txt";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    dgv_CANTrace.Rows.Clear();
                    tb_scriptName.Text = Path.GetFileName(openFileDialog.FileName.Replace(".txt",""));
                    List<CANCommand> cmdList = Utility.readScriptFile(openFileDialog.FileName);
                    foreach (CANCommand c in cmdList)
                        dgv_CANTrace.Rows.Insert(0, c.ID.ToString("X3"), c.DLC, c.Flags, c.DataBytes.d0.ToString("X2"), c.DataBytes.d1.ToString("X2"), c.DataBytes.d2.ToString("X2"), c.DataBytes.d3.ToString("X2"), c.DataBytes.d4.ToString("X2"), c.DataBytes.d5.ToString("X2"), c.DataBytes.d6.ToString("X2"), c.DataBytes.d7.ToString("X2"), c.TimeStamp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_SaveScript_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utility.saveScript(dgv_CANTrace, tb_scriptName.Text))
                {
                    MessageBox.Show("Script file saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                }
                else
                    MessageBox.Show("Error while saving script file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_scriptName_Click(object sender, EventArgs e)
        {
            try
            {
                tb_scriptName.Text = "";
                tb_scriptName.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_scriptName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (tb_scriptName.Text.Equals(""))
                {
                    tb_scriptName.ForeColor = SystemColors.ControlDark;
                    tb_scriptName.Text = "Script Name";
                }
                else
                    tb_scriptName.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
