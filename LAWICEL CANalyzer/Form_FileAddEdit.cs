﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LAWICEL_CANalyzer
{
    public partial class Form_FileAddEdit : Form
    {
        private string subDirectory = "";
        private bool fileOverwrite = false;
        public Form_FileAddEdit(string formTitle)
        {
            try
            {
                InitializeComponent();
                setForm(formTitle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setForm(string formTitle)
        {
            try
            {
                this.Text = formTitle;
                switch (formTitle)
                {
                    #region script
                    case "Create Script":
                        btn_Open.Visible = false;
                        dgv_CANMessages.Columns["Description"].Visible = false;
                        subDirectory = @"\Scripts";
                        fileOverwrite = false;
                        break;
                    case "Edit Script":
                        btn_Open.Visible = true;
                        dgv_CANMessages.Columns["Description"].Visible = false;
                        subDirectory = @"\Scripts";
                        fileOverwrite = true;
                        break;
                    #endregion
                    #region CommandList
                    case "Create Command List":
                        toolStripAddMessages.Visible = false;
                        dgv_CANMessages.Columns["Timestamp"].Visible = false;
                        dgv_CANMessages.Rows.Clear();
                        for (int i = 0; i < 10; i++)
                            dgv_CANMessages.Rows.Insert(0, "", "", "", "", "", "", "", "", "", "", "", "");
                        subDirectory = @"\Commands";
                        fileOverwrite = false;
                        break;
                    case "Edit Command List":
                        btn_Open.Visible = true;
                        dgv_CANMessages.Columns["Timestamp"].Visible = false;
                        subDirectory = @"\Commands";
                        fileOverwrite = true;
                        break;
                        #endregion
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddMessage_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_AddMessage())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        dgv_CANMessages.Rows.Insert(0, form.msg.ID.ToString("X3"), form.msg.DLC, form.msg.Flags, form.msg.DataBytes.d0.ToString("X2"), form.msg.DataBytes.d1.ToString("X2"), form.msg.DataBytes.d2.ToString("X2"), form.msg.DataBytes.d3.ToString("X2"), form.msg.DataBytes.d4.ToString("X2"), form.msg.DataBytes.d5.ToString("X2"), form.msg.DataBytes.d6.ToString("X2"), form.msg.DataBytes.d7.ToString("X2"), form.msg.TimeStamp, form.msg.Description);
                    }
                    form.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteMessage_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_CANMessages.Rows.Remove(dgv_CANMessages.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Open_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.InitialDirectory = Environment.CurrentDirectory + subDirectory;
                openFileDialog.Filter = "File (*.txt)|*.txt";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    dgv_CANMessages.Rows.Clear();
                    tb_fileName.Text = Path.GetFileName(openFileDialog.FileName.Replace(".txt",""));
                    List<CANCommand> cmdList = Utility.readCanCommandFile(openFileDialog.FileName);
                    foreach (CANCommand c in cmdList)
                        dgv_CANMessages.Rows.Insert(0, c.ID.ToString("X3"), c.DLC, c.Flags, c.DataBytes.d0.ToString("X2"), c.DataBytes.d1.ToString("X2"), c.DataBytes.d2.ToString("X2"), c.DataBytes.d3.ToString("X2"), c.DataBytes.d4.ToString("X2"), c.DataBytes.d5.ToString("X2"), c.DataBytes.d6.ToString("X2"), c.DataBytes.d7.ToString("X2"), c.TimeStamp, c.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            try
            {
                //garantisco che la dgv_CANMessages sia aggiornata con le prossime due istruzioni
                dgv_CANMessages.CurrentCell = dgv_CANMessages.Rows[0].Cells[1];
                dgv_CANMessages.CurrentCell = dgv_CANMessages.Rows[0].Cells[0];
                if (Utility.saveFile(dgv_CANMessages, tb_fileName.Text, subDirectory, fileOverwrite))
                {
                    MessageBox.Show("File saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                }
                else
                    MessageBox.Show("Error while saving File", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_fileName_Click(object sender, EventArgs e)
        {
            try
            {
                tb_fileName.Text = "";
                tb_fileName.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_fileName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (tb_fileName.Text.Equals(""))
                {
                    tb_fileName.ForeColor = SystemColors.ControlDark;
                    tb_fileName.Text = "File Name";
                }
                else
                    tb_fileName.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
