﻿namespace LAWICEL_CANalyzer
{
    partial class Form_Filter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Filter));
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_Submit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_MaxValue = new System.Windows.Forms.MaskedTextBox();
            this.tb_MinValue = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_Condition = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_AddCondition = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rb_Hex = new System.Windows.Forms.RadioButton();
            this.rb_Dec = new System.Windows.Forms.RadioButton();
            this.dgv_passRange = new System.Windows.Forms.DataGridView();
            this.feature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_GoToPass = new System.Windows.Forms.Button();
            this.btn_GoToStop = new System.Windows.Forms.Button();
            this.dgv_stopRange = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stopRange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stopRange)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Cancel.ImageKey = "cancel.png";
            this.btn_Cancel.ImageList = this.imageList;
            this.btn_Cancel.Location = new System.Drawing.Point(284, 406);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_Cancel.TabIndex = 11;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "submit.png");
            this.imageList.Images.SetKeyName(1, "addScript.png");
            this.imageList.Images.SetKeyName(2, "start.png");
            this.imageList.Images.SetKeyName(3, "stop.png");
            this.imageList.Images.SetKeyName(4, "openFiles.png");
            this.imageList.Images.SetKeyName(5, "save.png");
            this.imageList.Images.SetKeyName(6, "cancel.png");
            this.imageList.Images.SetKeyName(7, "ok.png");
            this.imageList.Images.SetKeyName(8, "nextArrow.png");
            this.imageList.Images.SetKeyName(9, "prevArrow.png");
            this.imageList.Images.SetKeyName(10, "delete.png");
            this.imageList.Images.SetKeyName(11, "rubber.png");
            this.imageList.Images.SetKeyName(12, "add.png");
            // 
            // btn_Submit
            // 
            this.btn_Submit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Submit.ImageKey = "ok.png";
            this.btn_Submit.ImageList = this.imageList;
            this.btn_Submit.Location = new System.Drawing.Point(142, 406);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(50, 50);
            this.btn_Submit.TabIndex = 10;
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_MaxValue);
            this.groupBox1.Controls.Add(this.tb_MinValue);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmb_Condition);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btn_AddCondition);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rb_Hex);
            this.groupBox1.Controls.Add(this.rb_Dec);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 86);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Filter Conditions";
            // 
            // tb_MaxValue
            // 
            this.tb_MaxValue.Location = new System.Drawing.Point(230, 39);
            this.tb_MaxValue.Mask = "&&&";
            this.tb_MaxValue.Name = "tb_MaxValue";
            this.tb_MaxValue.Size = new System.Drawing.Size(70, 20);
            this.tb_MaxValue.TabIndex = 3;
            // 
            // tb_MinValue
            // 
            this.tb_MinValue.Location = new System.Drawing.Point(140, 39);
            this.tb_MinValue.Mask = "&&&";
            this.tb_MinValue.Name = "tb_MinValue";
            this.tb_MinValue.Size = new System.Drawing.Size(70, 20);
            this.tb_MinValue.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Max Value";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(148, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Min Value";
            // 
            // cmb_Condition
            // 
            this.cmb_Condition.FormattingEnabled = true;
            this.cmb_Condition.Items.AddRange(new object[] {
            "Pass",
            "Stop"});
            this.cmb_Condition.Location = new System.Drawing.Point(49, 39);
            this.cmb_Condition.Name = "cmb_Condition";
            this.cmb_Condition.Size = new System.Drawing.Size(70, 21);
            this.cmb_Condition.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Condition";
            // 
            // btn_AddCondition
            // 
            this.btn_AddCondition.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddCondition.ImageKey = "add.png";
            this.btn_AddCondition.ImageList = this.imageList;
            this.btn_AddCondition.Location = new System.Drawing.Point(379, 22);
            this.btn_AddCondition.Name = "btn_AddCondition";
            this.btn_AddCondition.Size = new System.Drawing.Size(50, 50);
            this.btn_AddCondition.TabIndex = 6;
            this.btn_AddCondition.UseVisualStyleBackColor = true;
            this.btn_AddCondition.Click += new System.EventHandler(this.Btn_AddCondition_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 22);
            this.label1.TabIndex = 22;
            this.label1.Text = "ID";
            // 
            // rb_Hex
            // 
            this.rb_Hex.AutoSize = true;
            this.rb_Hex.Checked = true;
            this.rb_Hex.Location = new System.Drawing.Point(319, 55);
            this.rb_Hex.Name = "rb_Hex";
            this.rb_Hex.Size = new System.Drawing.Size(47, 17);
            this.rb_Hex.TabIndex = 5;
            this.rb_Hex.TabStop = true;
            this.rb_Hex.Text = "HEX";
            this.rb_Hex.UseVisualStyleBackColor = true;
            this.rb_Hex.CheckedChanged += new System.EventHandler(this.Rb_Hex_CheckedChanged);
            // 
            // rb_Dec
            // 
            this.rb_Dec.AutoSize = true;
            this.rb_Dec.Location = new System.Drawing.Point(319, 22);
            this.rb_Dec.Name = "rb_Dec";
            this.rb_Dec.Size = new System.Drawing.Size(47, 17);
            this.rb_Dec.TabIndex = 4;
            this.rb_Dec.Text = "DEC";
            this.rb_Dec.UseVisualStyleBackColor = true;
            this.rb_Dec.CheckedChanged += new System.EventHandler(this.Rb_Dec_CheckedChanged);
            // 
            // dgv_passRange
            // 
            this.dgv_passRange.AllowUserToAddRows = false;
            this.dgv_passRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_passRange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.feature,
            this.passRange});
            this.dgv_passRange.Location = new System.Drawing.Point(12, 104);
            this.dgv_passRange.Name = "dgv_passRange";
            this.dgv_passRange.ReadOnly = true;
            this.dgv_passRange.RowHeadersVisible = false;
            this.dgv_passRange.Size = new System.Drawing.Size(180, 271);
            this.dgv_passRange.TabIndex = 18;
            // 
            // feature
            // 
            this.feature.HeaderText = "";
            this.feature.Name = "feature";
            this.feature.ReadOnly = true;
            this.feature.Width = 40;
            // 
            // passRange
            // 
            this.passRange.DataPropertyName = "Timestamp";
            this.passRange.HeaderText = "Pass";
            this.passRange.Name = "passRange";
            this.passRange.ReadOnly = true;
            this.passRange.Width = 120;
            // 
            // btn_GoToPass
            // 
            this.btn_GoToPass.ImageKey = "prevArrow.png";
            this.btn_GoToPass.ImageList = this.imageList;
            this.btn_GoToPass.Location = new System.Drawing.Point(213, 289);
            this.btn_GoToPass.Name = "btn_GoToPass";
            this.btn_GoToPass.Size = new System.Drawing.Size(50, 50);
            this.btn_GoToPass.TabIndex = 9;
            this.btn_GoToPass.UseVisualStyleBackColor = true;
            this.btn_GoToPass.Click += new System.EventHandler(this.Btn_GoToPass_Click);
            // 
            // btn_GoToStop
            // 
            this.btn_GoToStop.ImageKey = "nextArrow.png";
            this.btn_GoToStop.ImageList = this.imageList;
            this.btn_GoToStop.Location = new System.Drawing.Point(213, 140);
            this.btn_GoToStop.Name = "btn_GoToStop";
            this.btn_GoToStop.Size = new System.Drawing.Size(50, 50);
            this.btn_GoToStop.TabIndex = 7;
            this.btn_GoToStop.UseVisualStyleBackColor = true;
            this.btn_GoToStop.Click += new System.EventHandler(this.Btn_GoToStop_Click);
            // 
            // dgv_stopRange
            // 
            this.dgv_stopRange.AllowUserToAddRows = false;
            this.dgv_stopRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_stopRange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.stopRange});
            this.dgv_stopRange.Location = new System.Drawing.Point(284, 104);
            this.dgv_stopRange.Name = "dgv_stopRange";
            this.dgv_stopRange.ReadOnly = true;
            this.dgv_stopRange.RowHeadersVisible = false;
            this.dgv_stopRange.Size = new System.Drawing.Size(172, 271);
            this.dgv_stopRange.TabIndex = 20;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // stopRange
            // 
            this.stopRange.HeaderText = "Stop";
            this.stopRange.Name = "stopRange";
            this.stopRange.ReadOnly = true;
            this.stopRange.Width = 120;
            // 
            // btn_Clear
            // 
            this.btn_Clear.ImageKey = "rubber.png";
            this.btn_Clear.ImageList = this.imageList;
            this.btn_Clear.Location = new System.Drawing.Point(213, 214);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(50, 50);
            this.btn_Clear.TabIndex = 8;
            this.btn_Clear.UseVisualStyleBackColor = true;
            // 
            // Form_Filter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 473);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.dgv_passRange);
            this.Controls.Add(this.btn_GoToPass);
            this.Controls.Add(this.btn_GoToStop);
            this.Controls.Add(this.dgv_stopRange);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Submit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Filter";
            this.Text = "ID Filter";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_passRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_stopRange)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_AddCondition;
        private System.Windows.Forms.RadioButton rb_Hex;
        private System.Windows.Forms.RadioButton rb_Dec;
        private System.Windows.Forms.ComboBox cmb_Condition;
        private System.Windows.Forms.DataGridView dgv_passRange;
        private System.Windows.Forms.Button btn_GoToPass;
        private System.Windows.Forms.Button btn_GoToStop;
        private System.Windows.Forms.DataGridView dgv_stopRange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn stopRange;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox tb_MaxValue;
        private System.Windows.Forms.MaskedTextBox tb_MinValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn feature;
        private System.Windows.Forms.DataGridViewTextBoxColumn passRange;
    }
}