﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace LAWICEL_CANalyzer
{
    public partial class Form_FileSelect : Form
    {
        public List<String> files;
        private string subDirectory = "";
        public Form_FileSelect(string subDirectory)
        {
            InitializeComponent();
            this.subDirectory = subDirectory;
            initializeDataGridViews(Environment.CurrentDirectory + subDirectory);
        }

        private void initializeDataGridViews(string path)
        {
            if (!Directory.GetFiles(path).Count().Equals(0))
                foreach (String file in Directory.GetFiles(path))
                    dgv_filesNotSelected.Rows.Insert(0, file.Replace(Environment.CurrentDirectory + subDirectory + @"\",""));
            if (!Directory.GetDirectories(path).Count().Equals(0))
                foreach (String dir in Directory.GetDirectories(path))
                    initializeDataGridViews(dir);
        }

        private void btn_Select_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_filesSelected.Rows.Add(dgv_filesNotSelected.CurrentRow.Cells[0].Value);
                dgv_filesNotSelected.Rows.Remove(dgv_filesNotSelected.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Deselect_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_filesNotSelected.Rows.Add(dgv_filesSelected.CurrentRow.Cells[0].Value);
                dgv_filesSelected.Rows.Remove(dgv_filesSelected.CurrentRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Submit_Click(object sender, EventArgs e)
        {
            try
            {
                files = new List<String>();
                foreach (DataGridViewRow r in dgv_filesSelected.Rows)
                    files.Add(r.Cells[0].Value.ToString());
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
