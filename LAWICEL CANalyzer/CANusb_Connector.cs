﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using Lawicel;
using System.Windows.Forms;
using System.Timers;
using NLog;
using NLog.Config;
using NLog.Targets;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace LAWICEL_CANalyzer
{
    public class CANusb_Connector : ICanReaderDelegate<CANCommand>
    {
        private uint connectorID;
        private string connectorName;
        private string baudRate;
        private Logger logger;
        private bool isConnected = false;
        private Thread CANusb_Reading_Thread = null;
        private Thread CANusb_Writing_Thread = null;
        private DateTime CANusb_startingConnectionTime;
        private TimeSpan CANusb_ElapsedTime;
        private CANCommand cmd = new CANCommand();

        private bool killListener = false;

        private ManualResetEvent msgsSendWait = new ManualResetEvent(false);
        private ConcurrentQueue<CANCommand> msgsToSendQueue = null;

        //metodi costruttori ed overoad

        public CANusb_Connector(string connectorName, string baudRate)
        {
            this.connectorName = connectorName;
            this.baudRate = baudRate;
            logger = LogManager.GetLogger("");
            this.CANusb_connect();
        }

        //accessors / muthators
        public Logger Logger { get { return logger; }}
        public uint ConnectorID { get { return connectorID; } }
        public string ConnectorName { get { return connectorName; } }
        public string BaudRate { get { return baudRate; } }
        public bool IsConnected { get { return isConnected; } }

        #region [ Fields / Attributes ]
        private Action _disposeAction;
        private Action<CANCommand> _delegates;

        private volatile bool _isDisposed;

        private readonly object _gateEvent = new object();
        #endregion


        #region [ Events / Properties ]
        public event Action<CANCommand> Handler
        {
            add
            {
                RegisterEventDelegate(value);
            }
            remove
            {
                UnRegisterEventDelegate(value);
            }
        }
        #endregion

        /*
         * Sezione registrazione event
         */

        private void CheckDisposed()
        {
            if (_isDisposed)
            {
                ThrowDisposed();
            }
        }

        private void ThrowDisposed()
        {
            throw new ObjectDisposedException(this.GetType().Name);
        }

        private void RegisterEventDelegate(Action<CANCommand> invoker)
        {
            if (invoker == null)
                throw new NullReferenceException("invoker");

            lock (_gateEvent)
            {
                CheckDisposed(); // check inside of lock because of disposable synchronization

                if (IsAlreadySubscribed(invoker))
                    return;

                AddActionInternal(invoker);
            }
        }

        private bool IsAlreadySubscribed(Action<CANCommand> invoker)
        {
            var current = _delegates;
            if (current == null)
                return false;

            var items = current.GetInvocationList();
            for (int i = items.Length; i-- > 0; )
            {
                if ((Action<CANCommand>)items[i] == invoker)
                    return true;
            }
            return false;
        }

        private void UnRegisterEventDelegate(Action<CANCommand> invoker)
        {
            if (invoker == null)
                return;

            lock (_gateEvent)
            {
                var baseVal = _delegates;
                if (baseVal == null)
                    return;

                RemoveActionInternal(invoker);
            }
        }

        private void AddActionInternal(Action<CANCommand> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal + invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal) // success
                    return;

                baseVal = currentVal;
            }
        }

        private void RemoveActionInternal(Action<CANCommand> invoker)
        {
            var baseVal = _delegates;
            while (true)
            {
                var newVal = baseVal - invoker;
                var currentVal = Interlocked.CompareExchange(ref _delegates, newVal, baseVal);

                if (currentVal == baseVal)
                    return;

                baseVal = currentVal; // Try again
            }
        }

        /*
         * Fine sezione registrazione event
         */

        public DateTime getConnectorStartingTime()
        {
            return (this.CANusb_startingConnectionTime);
        }

        public TimeSpan getConnectorElapsedTime()
        {
            return (this.CANusb_ElapsedTime);
        }


        /*Gestione Dispositivi CANusb*/

        public void CANusb_connect()
        {
            logger.Trace(logger.Name + " ***** Log Trace Started *****");
            this.connectorID = (CANUSB.canusb_Open(ConnectorName,
            BaudRate,
            CANUSB.CANUSB_ACCEPTANCE_CODE_ALL,
            CANUSB.CANUSB_ACCEPTANCE_MASK_ALL,
            CANUSB.CANUSB_FLAG_TIMESTAMP));
            if (this.connectorID > 0)
            {
                this.isConnected = true;
                killListener = false;
                CANusb_startingConnectionTime = DateTime.Now;
                this.CANusb_Reading_Thread = new Thread(new ThreadStart(this.threadPoolingMsg));//timer usato per la lettura dei messaggi lungo il canale CANBUS

                CANusb_Reading_Thread.IsBackground = true;
                CANusb_Reading_Thread.Start();

                this.msgsToSendQueue = new ConcurrentQueue<CANCommand>();
                this.msgsSendWait.Reset();
                this.CANusb_Writing_Thread = new Thread(new ThreadStart(this.processSendMessage));
                CANusb_Writing_Thread.IsBackground = true;
                this.CANusb_Writing_Thread.Start();
            }
            else
                this.isConnected = false;
        }

        public void CANusb_disconnect()
        {
            if (CANusb_Reading_Thread == null && CANusb_Writing_Thread == null)
                this.connectorID = 1;

            killListener = true;

            if (CANusb_Reading_Thread  != null)
                CANusb_Reading_Thread.Interrupt();
            CANusb_Reading_Thread = null;

            this.msgsSendWait.Set();
            if (CANusb_Writing_Thread != null)
                CANusb_Writing_Thread.Interrupt();

            CANusb_Writing_Thread = null;

            this.connectorID = (uint) CANUSB.canusb_Close(this.connectorID);
            if (this.connectorID.Equals(1))
                isConnected = false;
            else
                isConnected = true;
            logger.Trace(logger.Name + " ***** Log Trace Stopped *****");
            NLog.LogManager.Shutdown();
        }

        /*Timer*/

        private void threadPoolingMsg()
        {
            CANusb_ElapsedTime = DateTime.Now.Subtract(getConnectorStartingTime());
            int iCont = 1;
            // Read one CANData message
            CANCommand ret;
            try
            {
                while (true && !killListener)
                {
                    ret = new CANCommand();
                    ret = readMessage(this.connectorID);

                    if (CANUSB.ERROR_CANUSB_OK == ret.RetCode)
                    {
                        logger.Trace(logger.Name + " Rx | " + "0x" + ret.ID.ToString("X3") + " " + ret.DLC.ToString() + " " + " " + ret.Flags.ToString() + " " + "0x" + ret.DataBytes.d0.ToString("X2") + " " + "0x" + ret.DataBytes.d1.ToString("X2") + " " + "0x" + ret.DataBytes.d2.ToString("X2") + " " + "0x" + ret.DataBytes.d3.ToString("X2") + " " + "0x" + ret.DataBytes.d4.ToString("X2") + " " + "0x" + ret.DataBytes.d5.ToString("X2") + " " + "0x" + ret.DataBytes.d6.ToString("X2") + " " + "0x" + ret.DataBytes.d7.ToString("X2"));
                        ret.ActDateTime = DateTime.Now;
                        emitMessageChanges(ret);
                    }
                    else if (CANUSB.ERROR_CANUSB_NO_MESSAGE == ret.RetCode)
                    {
                        iCont = 0;
                    }
                    else
                    {
                        iCont = 0;
                        //logger.Error("Failed to read message");
                    }

                    Thread.Sleep(10);
                }
                
            }
            catch (ThreadInterruptedException ex)
            {
                //logger.Trace(logger.Name + " ***** Log Trace Halted *****");
            }

        }

        /*PURPOSE : reads messages retrieved by the CANusb connector*/
        private CANCommand readMessage(uint handle)
        {
            CANCommand ret = new CANCommand();
            CANUSB.CANMsg msg = new CANUSB.CANMsg();
            //msg.data = ret.getDataUlong();
            msg.data = ret.DataULong;
            CANUSB.canusb_Flush(ConnectorID, 0x00);
            int rv = CANUSB.canusb_Read(handle, out msg);
            if (CANUSB.ERROR_CANUSB_OK == rv)
            {
                ret.RetCode = rv;
                ret.ID = msg.id;
                ret.Flags = msg.flags;
                ret.DLC = msg.len;
                ret.DataULong = msg.data;
                ret.cleanData();
                ret.TimeStamp = msg.timestamp;
                ret.MessageType = (int) CANCommand.MexType.Rx;
            }
            return ret;
        }

        public void enqueueMessage(CANCommand msgSend)
        {
            //logger.Info("request enqueue message");
            if (this.msgsToSendQueue != null)
            {
                //logger.Trace(logger.Name + " Enqueue message: | " + "0x" + msgSend.getID().ToString("X3") + " " + msgSend.getDLC().ToString() + " " + "0x" + msgSend.getDatum(0).ToString("X2") + " " + "0x" + msgSend.getDatum(1).ToString("X2") + " " + "0x" + msgSend.getDatum(2).ToString("X2") + " " + "0x" + msgSend.getDatum(3).ToString("X2") + " " + "0x" + msgSend.getDatum(4).ToString("X2") + " " + "0x" + msgSend.getDatum(5).ToString("X2") + " " + "0x" + msgSend.getDatum(6).ToString("X2") + " " + "0x" + msgSend.getDatum(7).ToString("X2"));
                this.msgsToSendQueue.Enqueue(msgSend);
                this.msgsSendWait.Set();
            }
                
        }

        private void processSendMessage()
        {
            if (this.msgsToSendQueue == null)
            {
                CANusb_Writing_Thread.Abort();
                return;
            }

            CANCommand msg;

            try
            {
                while (true && !killListener)
                {
                    this.msgsSendWait.WaitOne();
                    
                    while (msgsToSendQueue.TryDequeue(out msg))
                    {
                        this.sendMessage(msg);
                        emitMessageChanges(msg);
                        Thread.Sleep(1);
                    }
                    this.msgsSendWait.Reset();
                }

            }
            catch (ThreadInterruptedException ex)
            {
                //logger.Trace(logger.Name + " Thread send halted");
            }
        }

        //Invia un messaggio al CANusb indicato
        private int sendMessage(CANCommand msgSend)
        {
            int rv;//return value
            CANUSB.CANMsg msg = new CANUSB.CANMsg();
            msg.id = msgSend.ID;
            msg.flags = msgSend.Flags;
            msg.len = msgSend.DLC;
            msg.data = msgSend.DataULong;
            msgSend.MessageType = (int)CANCommand.MexType.Tx;
            rv = CANUSB.canusb_Write(this.connectorID, ref msg);
            logger.Trace(logger.Name + " Tx | " + "0x" + msgSend.ID.ToString("X3") + " " + msgSend.DLC.ToString() + " " + msgSend.Flags.ToString() + " " + "0x" + msgSend.DataBytes.d0.ToString("X2") + " " + "0x" + msgSend.DataBytes.d1.ToString("X2") + " " + "0x" + msgSend.DataBytes.d2.ToString("X2") + " " + "0x" + msgSend.DataBytes.d3.ToString("X2") + " " + "0x" + msgSend.DataBytes.d4.ToString("X2") + " " + "0x" + msgSend.DataBytes.d5.ToString("X2") + " " + "0x" + msgSend.DataBytes.d6.ToString("X2") + " " + "0x" + msgSend.DataBytes.d7.ToString("X2"));
            msgSend.ActDateTime = DateTime.Now;
            return rv;
        }

        private void emitMessageChanges(CANCommand value)
        {
            CheckDisposed();
            var current = _delegates;
            if (current != null)
            {
                current(value);
            }
            else
                ; //logger.Warn(logger.Name + " processor messages not available ");
        }
    }
}
